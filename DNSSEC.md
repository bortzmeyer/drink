# Drink and DNSSEC

Drink answers can be secured with DNSSEC. The signatures are
dynamically generated since we don't know the responses in
advance. This file documents how to enable DNSSEC security. You need
to create cryptographic keys, then to run Drink with the proper
options.

## Creating keys

*Warning*: Drink currently requires RSA keys. (This is partially a
limit of the crypto library we use, see issue #49.)

Let's assume the domain is `test`. We use programs that are part of
[BIND](https://en.wikipedia.org/wiki/BIND) (package `bind9-utils` with Debian/Ubuntu/Mint).

```
dnssec-keygen -a RSASHA256 -b 2048 -f KSK test
```

To get the [DS
record](https://en.wikipedia.org/wiki/List_of_DNS_record_types#DS)
that some registries will require. It will also get you the keytag
(that Drink cannot compute, you have to give it):

```
dnssec-dsfromkey KEYNAME.key > ds.dns 
```

(You can also just look at the name of the files produced by
`dnssec-keygen`…)

Drink requires keys in
[PEM](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail) format. To
translate, we'll use a program from
[SoftHSM](https://en.wikipedia.org/wiki/OpenDNSSEC) (Debian package
`softhsm2`, it is also possible that the keymgr program in Knot-DNS
will work):

```
softhsm2-keyconv --in KEYNAME.private --out KEYNAME.pem 
```

## Enabling DNSSEC

From the command line, you have to use the options `--dnssec-key` and
`--dnssec-key-tag`. The first one takes as argument the name of a PEM
file (see the previous section) contaning the key. The second one takes
as argument the key tag (see the previous section, sorry, Drink cannot
yet compute the key tag itself).

If you instead use the configuration file, the options are
`dnssec-key` and `dnssec-key-tag`. Unlike the options on the command
line, they can take a list as value, if you manage several domains. Se
the `example.toml` file for details and examples.

## About the way it is done

While it is relatively easy to sign positive answers, negative
responses such as NODATA (NOERROR but ANSWER=0) or NXDOMAIN are more
complicated, since there are no RR (Resource Records) to sign. The
DNSSEC solution is to create NSEC records indicating that there is
nothing between this name and this other name (and they are then
signed). Because the names are not known in advance (for instance for
unit conversions or artithmetic services), we also generate the NSEC
dynamically. This can be done in several ways, we use the "white lies"
of RFC 4470. The idea if, for a name which does not exist, to generate
a NSEC record which goes from "just before" the non-existing name to
"just after".

Other zones use white lies, for instance at UltraDNS (see
`ultratest.huque.com`).

[Another solution would have been [black
lies](https://blog.cloudflare.com/black-lies/), which are not [too
difficult to
implement](https://arash-cordi.medium.com/implementing-dnssec-using-black-lies-4ae218c14f10)
but violate the usual semantics of NXDOMAIN (see Internet drafts
`draft-valsorda-dnsop-black-lies` and
`draft-huque-dnsop-blacklies-ent`).]




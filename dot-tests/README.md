# Create your certificate

# ECDSA

```
openssl ecparam -out key.pem -name prime256v1 -genkey
openssl req -new -key key.pem  -nodes -out server.crt -subj /CN=ns1.example.net
openssl x509 -in server.crt -out server.pem -req -signkey key.pem 
```

## RSA

```
openssl req -newkey rsa:2048 -nodes -keyout server.key -new -out server.crt -subj /CN=ns1.example.net -sha256
openssl rsa -in server.key -out key.pem
openssl x509 -in server.crt -out server.pem -req -signkey key.pem -days 2001
```


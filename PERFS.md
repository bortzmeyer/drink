# Performance and stress load


## dnsperf

https://github.com/DNS-OARC/dnsperf https://www.dns-oarc.net/tools/dnsperf 

Example of a data file:

```
% cat data
ip.test A
hello.test TXT
test SOA
```

Basic use (`-n` is the number of times you run the data file):

```
% dnsperf -n 10000 -c 100  -s 192.0.2.1 -p 3553 -d data
DNS Performance Testing Tool
Version 2.9.0

[Status] Command line: dnsperf -n 10000 -c 100 -s 192.0.2.1 -p 3553 -d data
[Status] Sending queries (to 192.0.2.1:3553)
[Status] Started at: Wed May  4 18:38:22 2022
[Status] Stopping after 10000 runs through file
[Status] Testing complete (end of file)

Statistics:

  Queries sent:         30000
  Queries completed:    30000 (100.00%)
  Queries lost:         0 (0.00%)

  Response codes:       NOERROR 30000 (100.00%)
  Average packet size:  request 25, response 82
  Run time (s):         11.389417
  Queries per second:   2634.024200

  Average Latency (s):  0.037506 (min 0.000736, max 0.181259)
  Latency StdDev (s):   0.017123
```

As always with performance measurements, the limiting factors can be
the processor (run dnsperf on a separate machine), the network (run
dnsperf on the same machine), etc. Increasing the number of "clients" (`-c` option) can overload Drink.

## Test TCP

```
dnsperf -n 10000 -c 100  -s 192.0.2.1 -p 3553 -d data -m tcp
```

### Test the number of TCP connections

By default, dnsperf reuses TCP connections. If we want to check the
ability of Drink to handle many connections, limit to one request per connection:


```
dnsperf -n 15000 -c 50 -s $SERVER -d drink.data -m tcp -O num-queries-per-conn=1 
```

Obviously, it dramatically reduces performances.

## Test DoT

```
dnsperf -n 10000 -c 100  -s 192.0.2.1 -p 8553 -d data -m dot
```



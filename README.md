# Drink

Drink is an experimental authoritative name server (DNS server), intended for
dynamic answers (answers depending on the client, on the date&time, on
the weather…) It is not a replacement for serious authoritative name
servers such as NSD or Knot. In its current state, it is quite rough
and unreliable.

## Installation

See `INSTALL.md`.

## Start

[You can also run Drink from Docker, see `Docker.md`.]

The (optional) configuration files are `$HOME/.drink.toml` or, if it
does not exist, `/etc/drink.toml`. It is in the
[TOML](https://github.com/toml-lang/toml) syntax. An example is in the
distribution, `example.toml`. When adding stuff at the end of the
file, *do not forget the final end-of-line* or you'll get nasty error
messages "%TomlElixir.Error{reason: ['syntax error before: ',". You
can choose an alternative configuration file with the `--config-file`
option or with the environment variable `DRINK_CONFIG`.

Just run `mix run drink.exs`, or, if you want HTTP client support,
`MIX_TARGET=http mix run drink.exs` (and `MIX_TARGET=full mix run
drink.exs` for the complete version). If you change the target, do not
forget to recompile everything with the same target, as mentioned in `INSTALL.md`.

You can override configuration file settings with command-line
options (for the default values, check `example.toml`) :

* `--config-file STRING` 
* `--base STRING` (or ` -b`, the base domain name)
* `--nameserver STRING` (or `-n`, the authoritative name server of the base domain, if it is inside of the base domain, you have to indicate the IP address(es) with "name.server.example=2001:db8::42::cafe")
* `--maintainer STRING` (or ` -m`, the email address of the maintainer of this service)
* `--service STRING` (or `-s`, the services - subdomains - to start; use several `--service` options to start several services)
* `--nsid STRING` (name server identity to announce in reply to NSID queries)
* `--port INTEGER` (or ` -p`, UDP and TCP port to listen on)
* `--bind IP-ADDRESS` (for IPv4, use the `::ffff:...` syntax; use several `--bind` options if you want to bind to multiple IP addresses)
* `--ipv4-only` (listens only on IPv4 addresses)
* `--ipv6-only` (listens only on IPv6 addresses - but the result will depend on some parameters of your operating system, it cannot be guaranteed)
* `--dot-cert STRING` (the file name of a certificate - in PEM format - for the DoT - DNS over TLS - service)
* `--dot-key STRING` (the file name of a private key - in PEM format - for the DoT - DNS over TLS - service)
* `--dot-port INTEGER` (the port to listen on for the DoT - DNS over TLS - service)
* `--no-padding` (never pad the responses)
* `--logging-level LEVEL` (or ` -l`, `LEVEL` being `info`, `debug`, `warning`, etc)
* `--logging-facility FACILITY` (such as `local1`, `local2`, etc)
* `--no-console` (no output on the console)
* `--statistics`(gather statistics that you may retrieve from the IPC system; this feature slows down the server but by a very few amount, as tested with dnsperf)
* `--ipc-socket PATH` (listen on this Unix socket, to receive commands)
* `--buffer-size INTEGER` (EDNS server buffer size, to control the maximum size of responses)
* `--negative-ttl INTEGER` (TTL of negative - for instance NXDOMAIN, No Such Domain - responses)
* `--default-ttl INTEGER` (TTL of some not-too-dynamic responses)
* `--secret-cookie STRING` (salt for the generation of DNS cookies)
* `--max-random INTEGER` (maximum value for random/TXT responses)

Stop the server with Control-C, then type `a` for abort. 

DNSSEC options are documented in `DNSSEC.md`.

### Talking to the server

If you used the `--ipc-socket` option (or have set `ipc-socket` in the
configuration file), you can send commands to the server with any
program that can talk to Unix sockets. (The socket is only usable by
the user who runs Drink.) One such program is netcat:

```
% netcat -U  ~/.drink.sock
status
Drink is running
foobar 
Unknown command "foobar"
[Control-C]
```

Another one is socat:

```
% echo version | socat - UNIX-CONNECT:/home/stephane/.drink.sock
Drink 0.8.0 DNS library version 2.4.0 Elixir version 1.12.2
```

The possible commands are:

* help
* status
* version
* statistics
* flush_statistics

## Usage

Drink is authoritative for a given DNS zone, the base domain, let's say `test`. It
answers to queries for this name (NS and TXT queries) and some names
underneath (depending on the `services` keyword in the configuration file, or on the ` --services` option(s)):

* `hello.test` returns a message with the version of the software (TXT queries)
* `ip.test` returns the IP address of the DNS client (A, AAAA and TXT queries, similar services can be found [https://www.bortzmeyer.org/drink.html](on my blog)
* `connection.test` returns the IP address and port of the DNS client, also with the transport protocol (TXT queries)
* `date.test` returns the date at the server (TXT queries)
* `random.test` returns a random integer (TXT queries) or IP address (A and AAAA queries) or location (LOC queries) or URL to OpenStreetMap with a random location (URI queries)
* `ecs.test` returns the EDNS Client Subnet information (TXT queries) 
* `country.test` returns the country of the DNS client (as much as geolocation works) (TXT queries)
* `bgp.test` returns the IP address of the DNS client, its IP prefix in the DFZ (default-free zone) and the origin AS (autonomous system) (TXT queries)
* `full.test` returns the IP address of the DNS client, its IP prefix in the DFZ, the origin AS and the country (TXT queries)
* `FROM.TO.unit.test` returns the result of the conversion of a number from an original unit to another one. Example: `32MB.KiB.unit.test` will return 32 megabytes converted to kibis. *Warning*: unit names are case-sensitive so it will not work if you go through a DNS forwarder which modifies the case. See [the list of possible units](https://github.com/carturoch/ex_uc).
* `EXPR.op.test` returns the result of the arithmetic expression on
  integers (such as 2+2 or 13*5 but also longer such as 2+3*5). It uses big integers so do not hesitate to indicate large numbers.
* `CITY.now.weather.test` returns the current weather for this city and `CITY.tomorrow.weather.test` returns the forecast for tomorrow (TXT queries)
* `NUM.LANG.number.test` returns the number NUM as spelled in the language LANG (TXT queries)

## Bug report

The best way to report an issue is on [the issue
tracker](https://framagit.org/bortzmeyer/drink/-/issues). Do not
forget to include the output of `mix run drink.exs --version` and of
`elixir --version`.

## Development

If you want to contribute to DRINK, see `DEVELOPMENT.md`.

## Author

[Stéphane Bortzmeyer](mailto:stephane+drink@bortzmeyer.org)

## Licence 

Free software, under the GPL v2. See `LICENSE`.


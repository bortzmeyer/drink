# Simple test of DNSSEC in Drink

We assume you use the keys in this  directory. (But you can
create your own keys as explained in `../DNSSEC.md`.)

Start Drink:

```
mix run drink.exs --dnssec-key dnssec-tests/Ktest.+008+18307.pem --dnssec-key-tag 18307 
```

Test with delv (also part of BIND):

```
delv @127.0.0.1 -p 3553 +root=test -a ./dnssec-tests/ta.bind -q test -t soa
```

Or with drill (part of ldns):

```
drill -S -k  dnssec-tests/Ktest.+008+18307.key @127.0.0.1 -p 3553  -t soa test
```

You can also test through a regular resolver. With [Unbound](), its
configuration will have:

```
server:
        trust-anchor: "test. [content of the key file]"

local-zone: test nodefault
stub-zone:
    name: "test"
    stub-addr: 127.0.0.1@3553
```

After that, you can add the DS record and test with tools like
[DNSviz](https://dnsviz.net/) or [Zonemaster](https://zonemaster.fr/).



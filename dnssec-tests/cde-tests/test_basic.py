#!/usr/bin/env python3

import pytest

import dns.rdatatype
import dns.message
import dns.query

timeout = 1

def test_noedns(domain, server, port):
    message = dns.message.make_query(domain, dns.rdatatype.SOA)
    message.use_edns(edns=False)
    response = dns.query.udp(message, server, port=port, timeout=timeout) 
    assert response.rcode() == dns.rcode.NOERROR and \
        (response.flags & dns.flags.AA) 
    assert response.edns == -1
            
def test_edns(domain, server, port):
    message = dns.message.make_query(domain, dns.rdatatype.SOA)
    message.use_edns(edns=True)
    response = dns.query.udp(message, server, port=port, timeout=timeout) 
    assert response.rcode() == dns.rcode.NOERROR and \
        (response.flags & dns.flags.AA) 
    assert response.edns == 0
            
def test_dnssec(domain, server, port):
    message = dns.message.make_query(domain, dns.rdatatype.SOA, want_dnssec=True)
    response = dns.query.udp(message, server, port=port, timeout=timeout)
    do_bit = (response.ednsflags & dns.flags.DO) == dns.flags.DO
    assert do_bit
    assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 2 and \
        response.answer[0].rdtype == dns.rdatatype.SOA and \
        response.answer[1].rdtype == dns.rdatatype.RRSIG
    

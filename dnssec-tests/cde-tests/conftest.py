def pytest_addoption(parser):
    parser.addoption("--domain", action="store", default="foobar.test")
    parser.addoption("--server", action="store", default="::1")
    parser.addoption("--port", action="store", default=3553)

def pytest_generate_tests(metafunc):
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixturenames".
    option_value = metafunc.config.option.domain
    if "domain" in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("domain", [option_value])
    option_value = metafunc.config.option.server
    if "server" in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("server", [option_value])
    option_value = metafunc.config.option.port
    if "port" in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("port", [int(option_value)])

            
        

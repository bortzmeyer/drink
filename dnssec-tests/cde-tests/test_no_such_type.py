#!/usr/bin/env python3

import pytest

import dns.rdatatype
import dns.message
import dns.query

import utils

timeout = 1

def test_noedns(domain, server, port):
    message = dns.message.make_query(domain, dns.rdatatype.MG)
    message.use_edns(edns=False)
    response = dns.query.udp(message, server, port=port, timeout=timeout) 
    assert response.rcode() == dns.rcode.NOERROR and \
        (response.flags & dns.flags.AA) 
    assert response.edns == -1
            
def test_edns(domain, server, port):
    message = dns.message.make_query(domain, dns.rdatatype.MG)
    message.use_edns(edns=True)
    response = dns.query.udp(message, server, port=port, timeout=timeout) 
    assert response.rcode() == dns.rcode.NOERROR and \
        (response.flags & dns.flags.AA) 
    assert response.edns == 0
            
def test_dnssec(domain, server, port):
    message = dns.message.make_query(domain, dns.rdatatype.MG, want_dnssec=True)
    response = dns.query.udp(message, server, port=port, timeout=timeout)
    do_bit = (response.ednsflags & dns.flags.DO) == dns.flags.DO
    assert do_bit
    assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0
    assert len(response.authority) == 4
    soa_set, nsec_set, ignore1, ignore2 = utils.get_rrsets(response.authority)
    assert str(nsec_set.name) == domain + "."
    nsec = nsec_set[0] # We assume only one NSEC in the RRset. May be
    # we should check.
    assert str(nsec.next) == "\\000." + domain + "."
    # We do not test the bitmap, because it depends on the types
    # actually present for this domain name.

#!/usr/bin/env python3

import pytest

import dns.rdatatype
import dns.message
import dns.query

import utils

timeout = 1

def test_noedns(domain, server, port):
    message = dns.message.make_query(domain, utils.nxname)
    message.use_edns(edns=False)
    response = dns.query.udp(message, server, port=port, timeout=timeout) 
    assert response.rcode() == dns.rcode.FORMERR and \
        (response.flags & dns.flags.AA) 
    assert response.edns == -1
            
def test_edns(domain, server, port):
    message = dns.message.make_query(domain, utils.nxname)
    message.use_edns(edns=True)
    response = dns.query.udp(message, server, port=port, timeout=timeout) 
    assert response.rcode() == dns.rcode.FORMERR and \
        (response.flags & dns.flags.AA) 
    assert response.edns == 0
            
def test_dnssec(domain, server, port):
    message = dns.message.make_query(domain, utils.nxname, want_dnssec=True)
    response = dns.query.udp(message, server, port=port, timeout=timeout)
    do_bit = (response.ednsflags & dns.flags.DO) == dns.flags.DO
    assert do_bit
    assert response.rcode() == dns.rcode.FORMERR and len(response.answer) == 0
    assert len(response.authority) == 0

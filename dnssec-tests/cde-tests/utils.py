import dns.rdatatype
import dns.tokenizer
import dns.rdtypes

nxname = 65283

def get_rrsets(section):
    soa = None
    soa_sig = None
    nsec = None
    nsec_sig = None
    for rrset in section:
        if rrset.rdtype == dns.rdatatype.SOA:
            if soa is not None:
                raise Exception("More than one SOA")
            else:
                soa = rrset
        elif rrset.rdtype == dns.rdatatype.NSEC:
            if nsec is not None:
                raise Exception("More than one NSEC")
            else:
                nsec = rrset
        elif rrset.rdtype == dns.rdatatype.RRSIG:
            if soa_sig is None:
                soa_sig = rrset # Actually, we don't make a difference
                                # between the signature of the SOA and
                                # the one of the NSEC. It doesn't
                                # matter for us.
            else:
                if nsec_sig is None:
                    nsec_sig = rrset
                else:
                    raise Exception("Too many signatures") # Questionable since we can have several signatures during a rollover
        else:
            raise Exception("Unknown type %s" % rrset.rdtype)
    if nsec is None:
        raise Exception("No NSEC")
    if soa is None:
        raise Exception("No SOA")
    return (soa, nsec, soa_sig, nsec_sig)

def make_bitmap(s):
    b = dns.rdtypes.util.Bitmap
    tok = dns.tokenizer.Tokenizer(s)
    return tuple(b.from_text(tok).windows)


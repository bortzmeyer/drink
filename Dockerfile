# syntax=docker/dockerfile:1

# Create an image for the Drink DNS server.

# https://hub.docker.com/_/elixir
FROM elixir:alpine

WORKDIR /drink

ENV MIX_TARGET=http
ENV MIX_ENV=prod

# bash is necessary because it is apparently hardwired in
#  capsh. libcap is necessary because the Alpine image does not include
#  capsh.
RUN apk update && apk upgrade && apk add bash libcap git build-base && git clone --depth 1 https://framagit.org/bortzmeyer/drink.git . && mix local.hex --force && mix local.rebar --force && mix deps.get && mix deps.compile && ./create-links.sh && mix compile && apk del git build-base && rm -rf /var/cache/apk/* && adduser -D drink

# Changing /root permissions should not be necessary but HOME is not
#  set properly and therefore Drink tries to read /root.drink.toml
#  (also, the Erlang VM tries to read /root/.config/erlang) :-(
RUN chmod a+x drink-docker.sh && chmod a+x /root

USER drink
RUN mix local.hex --force

USER root

# Create a new stage with just the Erlang runtime? ('FROM
#  alpine-erlang-base')? The image is quite big, partly because of
#  Elixir stuff that is useful only for compilation.

ENTRYPOINT ["./drink-docker.sh"]

CMD [] # No default parameters


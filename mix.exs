defmodule Drink.MixProject do
  use Mix.Project

  def project do
    [
      app: :drink,
      version: "1.2.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      homepage_url: "https://framagit.org/bortzmeyer/drink",
      source_url: "https://framagit.org/bortzmeyer/drink.git",
      escript: [main_module: Drink.Cli]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :ssl]
    ]
  end

  defp deps do
    [
      # Mandatory dependencies
      {:dns, ">= 2.4.0", git: "https://github.com/bortzmeyer/elixir-dns", branch: "Authority"}, # Original
      # at https://hexdocs.pm/dns/
      # https://github.com/tungd/elixir-dns/ but at least one serious
      # bug so fork was necessary.
      {:domainname, "~> 0.1.5"}, # https://hex.pm/packages/domainname https://framagit.org/bortzmeyer/domainname-elixir
      # During development of :domainname:
      # {:domainname, path: "../domainname"},
      {:toml_elixir, "~> 2.0.0"}, # https://hexdocs.pm/toml_elixir/
				  # https://github.com/nikolauska/toml_elixir,
				  # note there is also a (different)
				  # https://github.com/bitwalker/toml-elixir/
      {:logger_syslog_backend, "~> 1.0"}, # https://github.com/pma/logger_syslog_backend
      {:binary, "~> 0.0.5"}, # https://hexdocs.pm/binary
      {:ex_uc, "~> 1.2"}, # https://github.com/carturoch/ex_uc
      {:ex_crypto, "~> 0.10.0"}, # https://hexdocs.pm/ex_crypto/ # https://github.com/ntrepid8/ex_crypto
      {:jason, "~> 1.2"},
      # Optional dependencies, depending on MIX_TARGET or other Mix options.
      {:httpoison, "~> 1.8", targets: [:http, :full]}, # Necessary for some
					      # services like the BGP
                                              # one. https://hexdocs.pm/httpoison/
      {:geoip, "~> 0.2.7", targets: [:http, :full]},
      {:ex_cldr_numbers, "~> 2.30", targets: :full},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false} # For Dialyzer static analysis https://hexdocs.pm/dialyxir/
    ]
  end
end

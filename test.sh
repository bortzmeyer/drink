#!/bin/sh

echo "Without HTTP"
mix test
echo ""

echo "With HTTP"
MIX_TARGET=http mix test

echo "Full"
MIX_TARGET=full mix test


# Contributing to Drink

Don't forget to read the license, in `LICENSE`.

If you don't know Elixir, the way to run a development version is
typically:

```
mix run drink.exs [--OPTIONS]
```

If you ran Drink with `mix run`, you will also have to type `a` for
abort, after the Control-C.

To build with HTTP support (necessary for services like bgp or
country), set the environment variable `MIX_TARGET` to `http`.

You can also build a standalone script:

```
mix escript.build

./drink --OPTIONS
```

## General rules

* If you added functions, if possible, also add automatic tests for these
  functions in `test/`. `mix test` will run all the internal tests and
  `pytest` the external tests (with a live server). The script
  `test.sh` runs `mix test` for all the build options possible.

* We use Dialyzer for static analysis of the code and type checking. If you add
  functions, it is highly recommended to write a typespec (`@spec`)
  before it. `mix dialyzer` will allow you to test it.
  
## Adding a service

To add a service (a subdomain of the base domain), you'll need to:

* Edit `responses.ex` to call the service, typically by adding
  cases to the `response` function

* Create a `lib/drink/responses/YOUR-SERVICE.ex` to actually implement
  the service (use existing modules in `lib/drink/responses` as a
  starting point)
  
* Don't forget to edit the code for BASE/TXT (in the definition of
  function `response`) for the documentation

* Add the service to the function `possible_services` of `defaults.ex`

* [Optional] Edit `defaults.ex` if you want it by default

* [Recommended] Edit `README.md` to document it

* [Recommended] Edit `example.toml` to document it

* [Recommended] Edit the tests under `test/`, both the Elixir ones
  (internal tests) and the Python ones (external tests).

## Adding an EDNS option

To add support for an EDNS option, you'll need to:

* Edit `ednscodes.ex` for the option number
 
* Edit `edns.ex` for the parsing of the option

* Edit `server.ex` to do something useful with the option. Remember
  that, while close, the `edns` data type and `options` data type are
  different. The first stores the actual EDNS options present in the
  request, the second the options we decided to use in the response.

* [Recommended] Edit the tests under `test/` 

## New record types

If you add support for a new resource record type (say, for
instance, SRV), you'll need to add its on-the-wire encoding in
`lib/drink/encoding.ex`. Otherwise, it won't work with DNSSEC.

## About version dependencies

Drink currently includes a `mix.lock` file to pin (lock) versions of
the many libraries it depends on. There is no obvious choice here:
pinning dependencies make builds more reproducible and predictable but
it can slow down the upgrade of versions, for instance, when there is
a security vulnerability. The Rust programming language recommends
[using version lock for applications but not for
libraries](https://doc.rust-lang.org/cargo/faq.html#why-do-binaries-have-cargolock-in-version-control-but-not-libraries),
which seems a sensible advive. Since Drink is an application, we keep
the `mix.lock` (you are of course welcome to modify it).

You can type `mix hex.outdated` to check the versions against the Hex
repository. The article ["A Guide to Secure Elixir Package Updates" by
Dimitrios Zorbas](https://blog.appsignal.com/2022/03/15/a-guide-to-secure-elixir-package-updates.html)
is very useful here.

## Interesting code/talks/articles

* [Intentionally Broken DNS](https://gitlab.rd.nic.fr/labs/ibdns), written in Elixir
* [Erlang for Authoritative DNS](https://www.thestrangeloop.com/2013/erlang-for-authoritative-dns.html)

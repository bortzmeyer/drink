# Installation

[You can also run Drink from Docker, see `Docker.md`.]

(Be sure you have [Elixir](https://elixir-lang.org/) and its Mix tool
installed. If Mix asks you if it should install Hex - the package
repository, say yes.) You also need, for the libraries we use, the
Erlang development files (on Debian, these are the packages
`erlang-dev erlang-src`.)

Create the requied symbolic links (see the source code for
explanations):

```
./create-links.sh
```

(Ignore the "Runtime terminating during boot...", it is spurious.)

Then (and be patient, it can be quite long):

```
mix deps.get --target=host
mix deps.compile
```

And say "yes" to questions about install mix and rebar.

This will build you Drink *without* HTTP client support, which is necessary
for some services like the BGP one or the weather one. If you want HTTP support (this
will seriously increase the number of dependencies):

```
mix deps.get --target=http
MIX_TARGET=http mix deps.compile
```

And if you want the full version with a lot of libraries, which is
necessary for the numbers service (warning, compilation will required
an Internet access, to download the language libraries, and compiling
them takes time), you have to use target `full`:

```
mix deps.get --target=full
MIX_TARGET=full mix deps.compile
```

Compiling may require a lot of memory, otherwise your process could be
killed by the operating system, if you have a small machine. On Linux, you may add temporary swap,
for instance with (to get two gibabytes):

```
dd if=/dev/zero of=/var/tmp/swap bs=1024 count=2000000
mkswap /var/tmp/swap
chmod 0600 /var/tmp/swap
chown root.root /var/tmp/swap
sudo swapon /var/tmp/swap
```

Do not forget to run all commands (`mix deps.get` and `mix
deps.compile`, as well as the later `mix run`) with the same target.

If you want to run Drink unattended on a machine with systemd, see `admin/drink.service`.

It is also possible to build a standalone script with `mix
escript.build` but it is not yet used in the examples.


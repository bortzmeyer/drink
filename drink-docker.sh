#!/bin/sh

# Linux only, sorry. For other kernels, if Docker runs on them, you
# will have to provide your own script.

# Not started by default, but necessary (otherwise, enoent on /dev/log)
/sbin/syslogd

# OK, this is complicated. Let's see why it is this way:
# * capsh is here to add the capability to bind to a priviledged port
#  (such as 53)
# * we use +eip because I was too lazy to read the documentation about
#   capabilities
# * cat_setpcap is to be able to add cap_setuid (and gid) which are
#   necessary to switch to an unprivileded user
# * exec is because we need to replace the shell, otherwise signals
#   are not transmitted to the Drink process
# WARNING: the order of parameters for capsh is significant.

exec capsh --caps="cap_net_bind_service+eip cap_setpcap+eip cap_setuid+eip cap_setgid+eip" --keep=1 --user=drink --addamb="cap_net_bind_service" -- -c "exec mix run --no-deps-check --no-compile drink.exs $*"

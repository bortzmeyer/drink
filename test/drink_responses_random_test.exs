defmodule DrinkResponsesRandomTest do
  use ExUnit.Case
  doctest Drink.Responses.Random
  test "a" do
    assert %{data: {_a, _b, _c, _d}, rcode: 0} = Drink.Responses.Random.response(:a)
  end
  test "aaaa" do
    assert %{data: {_a, _b, _c, _d, _e, _f, _g, _h}, rcode: 0} = Drink.Responses.Random.response(:aaaa)
  end
  test "txt" do
    %{data: s, rcode: r} = Drink.Responses.Random.response(:txt)
    assert r == 0 and is_list(s) and is_list(Enum.at(s, 0))
  end
  test "srv" do
    assert %{rcode: 0} = Drink.Responses.Random.response(:srv)
  end
end


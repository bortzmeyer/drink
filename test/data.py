# Misc. data for the tests

import os
import tempfile

run_timeout = 2.0
ipc_socket = "/tmp/test-drink.sock" # tempfile.TemporaryDirectory().name
                                    # + "/.test-drink.sock" would be
                                    # safer but we would have to
                                    # communicate that to the server.

if "DRINK_PROD" in os.environ:
    server = "193.70.85.11"
    me = None
    timeout = 1.25
    expectations = {"prod": {"domain": "dyn.bortzmeyer.fr", "port": 53, "auth": "ns1-dyn.bortzmeyer.fr",
                             "nsid": b"radia.bortzmeyer.org", "key": None, # Key found in the DNS
                             "auth-ip": None}}
    http_configs = ["prod"]
    unit_configs = ["prod"]
    dnssec_configs = ["prod"] 
    unsigned_configs = []
    ipc_configs = []
    dot_configs = []
    stats_configs = []
    weather_configs = ["prod"]
    number_configs = ["prod"]
elif "DRINK_PROD2" in os.environ:
    server = "2a01:e34:ec43:e1d0:40d8:4aff:fec2:6570"
    me = None
    timeout = 1.25
    expectations = {"prod": {"domain": "dyn.sources.org", "port": 53, "dot-port": 853, "auth": "ns1.dyn.sources.org",
                             "nsid": b"ns1.dyn.sources.org", "auth-ip": "2a01:e34:ec43:e1d0:40d8:4aff:fec2:6570", "key": None, # Key found in the DNS
                             "subject": "ns1.dyn.sources.org"}}
    http_configs = ["prod"]
    unit_configs = ["prod"]
    dnssec_configs = ["prod"] 
    unsigned_configs = []
    ipc_configs = []
    dot_configs = ["prod"]
    stats_configs = []
    weather_configs = []
    number_configs = []
elif "DRINK_DOCKER" in os.environ:
    # Start the Docker image with 'docker run -p 3553:53 -p
    # 3553:53/udp IMAGE-ID'
    server = "172.17.0.1" # Docker does not see the real IP address
    me = "172.17.0.1"
    timeout = 1.0
    expectations = {"prod": {"domain": "test", "port": 3553, "auth": "ns1.test",
                             "nsid": b"ns1.test", "auth-ip": "::1"}}
    http_configs = ["prod"]
    dnssec_configs = []  # Not in Docker yet
    unsigned_configs = ["prod"]
    unit_configs = ["prod"]
    ipc_configs = []
    dot_configs = []
    stats_configs = []
    weather_configs = []
    number_configs = []
else:
    server = "::1"
    me = "::1"
    timeout = 0.75
    http_env = os.environ.copy()
    http_env["MIX_TARGET"] = "http"
    full_env = http_env
    full_env["MIX_TARGET"] = "full"
    expectations = {
        "config1": {"domain": "drink1.test", "port": 3553, "auth": "ns1.drink1.test", "auth-ip": "2001:db8:1::bad:c0ff:ee",
                    "nsid": b"SECRET", "environ": http_env},
        "config2": {"domain": "drink2.test", "port": 3554, "auth": "ns1.example",  "auth-ip": None, "nsid": b"ns1.example"},
        "config3": {"domain": "drink3.test", "port": 3555, "auth": "ns1.drink3.test",  "auth-ip": "192.0.2.1", "nsid": b"SECRET", "key": "Kdrink3.test.+008+63610",
                    "environ": http_env},
        "config4": {"domain": "drink4.test", "port": 3556, "auth": "ns1.drink1.test", "auth-ip": None,
                    "nsid": b"ns1.drink1.test", "key": "Kdrink4.test.+008+33709", "environ": http_env},
        "config5": {"domain": "drink5.test", "port": 3557, "dot-port": 8557, "auth": "ns1.example.net", "auth-ip": None,
                    "nsid": b"SECRET", "environ": http_env,
                    "subject": "ns1.example.net"},
        "config6": {"domain": "test", "port": 3558, "auth": "ns1.example.net", "auth-ip": None,
                    "nsid": b"ns1.example.net", "key": "Ktest.+008+18307",
                    "environ": http_env}, # Domain is a TLD because some bugs happened only for
                    # one-label domain names. See config3 and 4 for DNSSEC with a SLD.
        "config7": {"domain": "drink7.test", "port": 3559, "auth": "ns1.example.net", "auth-ip": None,
                    "key": "Kdrink7.test.+008+60755",
                    "nsid": b"SECRET", "environ": full_env,
                    "subject": "ns1.example.net"}
        }
    http_configs = ["config1", "config3", "config4", "config5", "config6", "config7"]
    unit_configs = ["config1", "config4"]
    dnssec_configs = ["config3", "config4", "config6", "config7"]
    unsigned_configs = ["config1", "config2", "config5"]
    ipc_configs = ["config3"] # Needs a recent Erlang otherwise "Erlang error: {:invalid, {:protocol, %{}}}"
    weather_configs = ["config1", "config3"]
    number_configs = ["config7"]
    dot_configs = ["config5"]
    stats_configs = ["config3"]
        
# Classes
in_class = 1

# RR types
ns = 2
soa = 6
txt = 16
opt = 41

# EDNS options
nsid = 3

# Return codes
noerror = 0
formerr = 1
nxdomain = 3

# Misc
bufsize = 1240

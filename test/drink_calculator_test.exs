defmodule DrinkCalculatorTest do
  use ExUnit.Case
  doctest Drink.Calculator

  test "plus1" do
    assert Drink.Calculator.call("2+2") == 4
  end

  test "plus2" do
    assert Drink.Calculator.call("2 + 2 ") == 4
  end

  test "plus3" do # Integer only
    assert Drink.Calculator.call("2.1 + 2.2 ") == 4
  end

  test "negative1" do
    assert Drink.Calculator.call("1 - 2") == -1
  end

  # Bug #44
  test "negative2" do
    # assert Drink.Calculator.call("(4-5)*2") == -2
    assert_raise ArgumentError, fn -> Drink.Calculator.call("(4-5)*2") end
  end

  # Bug #44
  test "startparen1" do
    # assert Drink.Calculator.call("( 2 + 3 ) * 2") == 10
    assert_raise ArgumentError, fn -> Drink.Calculator.call("( 2 + 3 ) * 2") end
  end
 
  # Bug #44
  test "startparen2" do
    # assert Drink.Calculator.call("0 + ( 2 + 3 ) * 2") == 10
    assert_raise ArgumentError, fn -> Drink.Calculator.call("0 + ( 2 + 3 ) * 2") end
  end
 
  test "div1" do
    assert Drink.Calculator.call("4 / 2 ") == 2
  end

  test "big1" do
    assert Drink.Calculator.call("2*4567456456567697679768") == 9134912913135395359536
  end

  test "error1" do
    assert_raise MatchError, fn -> Drink.Calculator.call("2 $ 2") end
  end

  test "error2" do
    assert_raise ArithmeticError, fn -> Drink.Calculator.call("2 / 0") end
  end

  test "originaltests" do
    assert 10 ==  Drink.Calculator.call("4+6")
    assert 24 == Drink.Calculator.call("4 * 6")
    assert 8 == Drink.Calculator.call("2 + 3 * 4 - 6")
    assert 34 == Drink.Calculator.call("4 + 5 * 6")
    assert 2 == Drink.Calculator.call("3 / 3 + 1")
    assert 2 == Drink.Calculator.call("3 / 3 + 4 / 4")
    #assert 8 == Drink.Calculator.call("4 * ( 4 - 2 )")
    #assert 8 == Drink.Calculator.call("4 * ( 6 - 2 * ( 4 - 2 ) )")
    #assert 27 == Drink.Calculator.call("4 / 2 + 8 * 4 - ( 5 + 2 )")
  end
  
end

#!/usr/bin/env python3

import sys
import struct
import socket
import re
import os

import pytest

# DNSpython <https://www.dnspython.org/> <https://dnspython.readthedocs.io/>
import dns.rdatatype
import dns.message
import dns.query

from data import expectations, timeout, server, number_configs
from utils import address_family

# Tests for ticket #21

NUM = 378

def test_basic():
    # WARNING: test will fail if there no outside connectivity.
    if "DRINK_PROD" in os.environ or ("DRINK_PROD2" not in os.environ and "DRINK_DOCKER" not in os.environ):
        for config in number_configs:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query(str(NUM) + ".fr.number." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=3*timeout) # The remote server can be slow.
            assert response.rcode() == dns.rcode.NOERROR and \
                response.answer[0][0].strings[0].decode().startswith("trois")


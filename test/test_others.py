#!/usr/bin/env python3

import sys
import struct
import socket
import re
import os

import pytest

# DNSpython <https://www.dnspython.org/> <https://dnspython.readthedocs.io/>
import dns.rdatatype
import dns.message
import dns.query

from data import expectations, timeout, server, me, http_configs, unit_configs, in_class, soa, txt
from utils import address_family

def test_basic():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        auth = expectations[config]["auth"]
        message = dns.message.make_query(domain, dns.rdatatype.SOA)
        message.use_edns(edns=False)
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR and \
            (response.flags & dns.flags.AA) 
        assert response.edns == -1
            
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        auth = expectations[config]["auth"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        message.use_edns(edns=True) # The answer can be larger than 512 bytes
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR and "Possible queries: " in str(response.answer[0][0])
        assert response.edns == 0

        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        message.use_edns(edns=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR and "Possible queries: " in str(response.answer[0][0])

        message = dns.message.make_query(domain, dns.rdatatype.SRV)
        message.use_edns(edns=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0 and \
             (response.flags & dns.flags.AA)
        
        # DNS is case-insensitive
        message = dns.message.make_query(domain.upper(), dns.rdatatype.TXT)
        message.use_edns(edns=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR and "Possible queries: " in str(response.answer[0][0])

        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        message.use_edns(edns=1) # EDNS version 1 does not exist yet
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.BADVERS

        message = dns.message.make_query(domain, dns.rdatatype.NS)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and auth in str(response.answer[0])

        message = dns.message.make_query(domain + "notmybusiness", dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.REFUSED

        message = dns.message.make_query(domain, dns.rdatatype.AXFR)
        response = dns.query.tcp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.REFUSED

        message = dns.message.make_query("doesnotexist." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NXDOMAIN and (response.flags & dns.flags.AA)
        auth_r = response.find_rrset(dns.message.AUTHORITY, dns.name.from_text(domain),
                                  in_class, soa)
        assert auth_r[0] != None # Bug #32

def test_nsid():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        nsid = expectations[config]["nsid"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        message.use_edns(payload=4096, options=[dns.edns.GenericOption(dns.edns.NSID, b"")])
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR 
        for option in response.options:
            if option.otype == dns.edns.NSID:
                # dnspython handles NSID with version >=
                # 2.6. Before that, we have to do it.
                if hasattr(option, "data"): # < 2.6
                    nsid_r = option.data
                elif hasattr(option, "nsid"): # > 2.6
                    nsid_r = option.to_text()
                    if nsid_r.startswith("NSID "):
                        nsid_r = nsid_r[5:] # Remove the "NSID " prefix
                    nsid_r = nsid_r.encode()
                else: # NSID option not found or could not be parsed
                    assert False, "Cannot retrieve the NSID value"
                assert nsid_r == nsid

def test_nameserver():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        auth = expectations[config]["auth"]
        auth_ip = expectations[config]["auth-ip"]
        if auth_ip is None:
            qtype = dns.rdatatype.AAAA
        elif address_family(auth_ip) == 4:
            qtype = dns.rdatatype.A
        elif  address_family(auth_ip) == 6:
            qtype = dns.rdatatype.AAAA
        message = dns.message.make_query(auth, qtype)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert (auth_ip is None and response.rcode() == dns.rcode.REFUSED) or \
            response.rcode() == dns.rcode.NOERROR and auth_ip in str(response.answer[0])
    
def test_mailserver():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        qtype = dns.rdatatype.MX
        message = dns.message.make_query(domain, qtype)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and str(response.answer[0][0].exchange) == "."
    
def test_cookie():
    my_cookie = b"deadcafe"
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        message.use_edns(payload=4096, options=[dns.edns.GenericOption(dns.edns.COOKIE, my_cookie)])
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR 
        for option in response.options:
            if option.otype == dns.edns.COOKIE:
                cookie = option.data
                assert cookie[0:len(my_cookie)] == my_cookie

def test_ede(): # Extended DNS error, RFC 8914
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain + "doesnotexist", dns.rdatatype.TXT)
        message.use_edns(payload=4096)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.REFUSED
        found = False
        for option in response.options:
            # EDE option was not known of dnspython in version 2.2, it
            # appeared later (2.2.1?). We have to test and do it
            # ourself with option type = 15 (Extended DNS Error Code)
            # if necessary.
            if option.otype == 15:
                found = True
                if "code" in option.__dict__:
                    # Recent dnspython
                    code = option.code
                else:
                    # Old dnspython
                    code = struct.unpack(">H", option.data)[0]
                assert code == 20 # Not Authoritative
        assert found
        
def test_date():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("date." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            re.search(r"^[0-9]{4,5}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]+Z$",
                      response.answer[0][0].strings[0].decode()) is not None

def test_bgp():
    # WARNING: test will fail if there no outside connectivity.
    if "DRINK_PROD2" not in os.environ and "DRINK_DOCKER" not in os.environ:
        for config in http_configs:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query("bgp." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=3*timeout) # The remote server can be slow.
            assert response.rcode() == dns.rcode.NOERROR and \
                (me is None or me == response.answer[0][0].strings[0].decode())

def test_unit():
    for config in unit_configs: 
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("32MB.KiB.unit." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and response.answer[0][0].strings[0].decode() == "31250.00 KiB"

def encode_name(s):
    labels = s.split(".")
    result = b""
    for label in labels:
        result = result + struct.pack("B", len(label)) + label.encode()
    return result + struct.pack("B", 0)

def test_mbz(): # Field "Z", Must Be Zero, RFC 1035, section 4.1.1. See bug #71.
    for config in expectations:
        if config != "config1":
            continue
        # Apparently no way to add a value to the Z field (like dig
        # does with +zflag) with dnspython so we build the packet manually:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        addr = socket.getaddrinfo(server, port)[0] # Yes, we take only the
                                                   # first result, it is a
                                                   # bit arbitrary.
        (family, type, proto, canonname, sockaddr) = addr
        s = socket.socket(family, socket.SOCK_DGRAM) 
        s.settimeout(timeout)
        id = 42
        misc = 112 # Opcode 0, value in the Z field
        data = struct.pack(">HHHHHH", id, misc, 1, 0, 0, 0) + \
            encode_name("hello." + domain) + struct.pack(">H", txt) + struct.pack(">H", in_class)
        s.sendto(data, sockaddr)
        rdata, remote_server = s.recvfrom(4096)
        resp = dns.message.from_wire(rdata)
        assert resp.rcode() == dns.rcode.NOERROR
        (id1, id2, misc1, misc2) = struct.unpack(">BBBB", rdata[0:4])
        assert misc2 == 0
        

defmodule DrinkParsingTest do
  use ExUnit.Case
  doctest Drink.Parsing

  @id 12345
  @request 0 # opcode 0, query, all flags zero
  @response 32768 # opcode 0, response, all flags zero
  # Query types
  @ns 2
  @mx 15
  @aaaa 28
  @opt 41
  # Classes
  @in_class 1
  # EDNS options
  @nsid 3

  test "empty1" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @ns::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)>>,
      false)
    {:ok, r} = result
    assert r[:qname] == "." and r[:qtype] == :ns and r[:version] == nil
  end

  test "good1" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length
      102::unsigned-integer-size(8), # Qname = "foo"
      111::unsigned-integer-size(8),
      111::unsigned-integer-size(8),
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @aaaa::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)>>,
      false)
    {:ok, r} = result
    assert r[:qname] == "foo" and r[:qtype] == :aaaa and r[:version] == nil
  end

  test "nonrequest1" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @response::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length
      102::unsigned-integer-size(8), # Qname = "foo"
      111::unsigned-integer-size(8),
      111::unsigned-integer-size(8),
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @aaaa::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)>>,
      false)
    assert result == {:error, :notQuery}
  end

  test "goodedns1" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      1::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length
      102::unsigned-integer-size(8), # Qname = "foo"
      111::unsigned-integer-size(8),
      111::unsigned-integer-size(8),
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16),
      # RFC 6891, section 6.1.2
      0::unsigned-integer-size(8), # Name (empty = the root)
      @opt::unsigned-integer-size(16),
      1111::unsigned-integer-size(16), # Buffer size
      0::unsigned-integer-size(32),
      0::unsigned-integer-size(16) # Zero EDNS options
      >>,
      false)
    {:ok, r} = result
    assert r[:qname] == "foo" and r[:qtype] == :mx and r[:version] == 0 and r[:size] == 1111
  end

  test "goodedns2" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      1::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length
      102::unsigned-integer-size(8), # Qname = "foo"
      111::unsigned-integer-size(8),
      111::unsigned-integer-size(8),
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16),
      # RFC 6891, section 6.1.2
      0::unsigned-integer-size(8), # Name (empty = the root)
      @opt::unsigned-integer-size(16),
      1111::unsigned-integer-size(16), # Buffer size
      0::unsigned-integer-size(32),
      4::unsigned-integer-size(16), # Length of EDNS options
      @nsid::unsigned-integer-size(16),
      0::unsigned-integer-size(16) # NSID is just a boolean, no option data
      >>,
      false)
    {:ok, r} = result
    assert r[:qname] == "foo" and r[:qtype] == :mx and r[:version] == 0 and r[:size] == 1111 and r[:edns][:nsid] 
  end

  test "bad1" do
    result = Drink.Parsing.parse_request(<<>>, false)
    assert result == {:error, :badDNS}
  end

  test "wrongcount1" do  # Section count ≠ actual size. RFC 9267, section 6
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16) # ARcount
      >>, # No section Question. A good opportunity to crash a program...
      false)
    assert result == {:error, :badDNS}
  end

  test "wrongcount2" do  # Qname length larger than actual qname
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length but...
      102::unsigned-integer-size(8) # Qname = "f", just one octet
      >>, 
      false)
    assert result == {:error, :badDNS}
  end

  test "pointeroutofbounds1" do # RFC 9267, section 2
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      0xff::unsigned-integer-size(8), 
      0xff::unsigned-integer-size(8),
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)
      >>, 
      false)
    assert result == {:error, :badDNS}    
  end
  
  test "pointerloop1" do # RFC 9267, section 2
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      0xc0::unsigned-integer-size(8), # First label of the question
				      # section starts with a
				      # compression pointer to itself.
      0x0c::unsigned-integer-size(8),
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)
      >>, 
      false)
    assert result == {:error, :badDNS}    
  end
  
  test "pointerloop2" do # RFC 9267, section 2
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      4::unsigned-integer-size(8),
      ?t::unsigned-integer-size(8), 
      ?e::unsigned-integer-size(8),
      ?s::unsigned-integer-size(8),
      ?t::unsigned-integer-size(8),
      0xc0::unsigned-integer-size(8), 
      0x0c::unsigned-integer-size(8), # Compression pointer going back to "test"
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)
      >>, 
      false)
    assert result == {:error, :badDNS}    
  end
  
  test "nonullterminated1" do # RFC 9267, section 4
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      0::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8),
      ?f::unsigned-integer-size(8), 
      ?o::unsigned-integer-size(8),
      ?o::unsigned-integer-size(8),
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16)
      >>, 
      false)
    assert result == {:error, :badDNS}    
  end

  # We do not test the wrong RDLENGTH (RFC 9267, section 5) since we
  # are an authoritative name server, we don't have RDATA.
  
  test "badedns1" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      1::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length
      102::unsigned-integer-size(8), # Qname = "foo"
      111::unsigned-integer-size(8),
      111::unsigned-integer-size(8),
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16),
      # RFC 6891, section 6.1.2
      0::unsigned-integer-size(8), # Name (empty = the root)
      @opt::unsigned-integer-size(16),
      1111::unsigned-integer-size(16), # Buffer size
      0::unsigned-integer-size(32),
      14::unsigned-integer-size(16), # Length of EDNS options. Note
				     # that we cheat (there are not 14
				     # bytes). 
      @nsid::unsigned-integer-size(16), 
      0::unsigned-integer-size(16) # NSID is just a boolean, no option data
      >>,
      false)
    assert result == {:error, :badDNS}
  end

  test "badedns2" do
    result = Drink.Parsing.parse_request(<<@id::unsigned-integer-size(16),
      @request::unsigned-integer-size(16),
      1::unsigned-integer-size(16), # QDcount
      0::unsigned-integer-size(16), # ANcount
      0::unsigned-integer-size(16), # NScount
      1::unsigned-integer-size(16), # ARcount
      3::unsigned-integer-size(8), # Qname length
      102::unsigned-integer-size(8), # Qname = "foo"
      111::unsigned-integer-size(8),
      111::unsigned-integer-size(8),
      0::unsigned-integer-size(8), # Qname (empty = the root)
      @mx::unsigned-integer-size(16),
      @in_class::unsigned-integer-size(16),
      # RFC 6891, section 6.1.2
      0::unsigned-integer-size(8), # Name (empty = the root)
      @opt::unsigned-integer-size(16),
      1111::unsigned-integer-size(16), # Buffer size
      0::unsigned-integer-size(32),
      4::unsigned-integer-size(16), # Length of EDNS options. 
      @nsid::unsigned-integer-size(16), 
      1::unsigned-integer-size(16) # We cheat here. NSID is just a boolean, no option data. See issue #33.
      >>,
      false)
    case result do
      {:problem, :badEDNSopt, _details} -> assert true
      _ -> assert false
    end
  end

end

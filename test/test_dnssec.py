from data import expectations, timeout, server, bufsize, dnssec_configs, unsigned_configs

import dns.message
import dns.query

def test_basic():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.NS, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        do_bit = (response.ednsflags & dns.flags.DO) == dns.flags.DO
        assert do_bit
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 2 and \
            response.answer[0].rdtype == dns.rdatatype.NS and \
            response.answer[1].rdtype == dns.rdatatype.RRSIG

def test_key():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.DNSKEY, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 2 and \
            response.answer[0].rdtype == dns.rdatatype.DNSKEY and \
            response.answer[1].rdtype == dns.rdatatype.RRSIG 
        message = dns.message.make_query("hello." + domain, dns.rdatatype.DNSKEY, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0

def test_no_dnssec():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT, payload=bufsize, want_dnssec=False)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 1 # TXT

def test_base_type_exists():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT, payload=bufsize, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 2 # TXT + RRSIG
        message = dns.message.make_query("hello." + domain, dns.rdatatype.TXT, payload=bufsize, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 2 # TXT + RRSIG

def test_base_type_does_not_exist():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.SRV, payload=bufsize, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0 and \
            len(response.authority) == 4 # SOA + RRSIG + NSEC + RRSIG

def test_unsigned():
    for config in unsigned_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.SOA, want_dnssec=True) 
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            len(response.answer) == 1 and response.answer[0].rdtype == dns.rdatatype.SOA and \
            len(response.authority) == 0

def test_no_do_bit():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.NS, use_edns=0, want_dnssec=False)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 1 and \
            response.answer[0].rdtype == dns.rdatatype.NS 
        do_bit = (response.ednsflags & dns.flags.DO) == dns.flags.DO
        assert not do_bit

def test_nsec_nodata():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.LOC, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0 and \
            len(response.authority) == 4 and \
            response.authority[0].rdtype == dns.rdatatype.SOA and \
            response.authority[1].rdtype == dns.rdatatype.RRSIG and \
            response.authority[2].rdtype == dns.rdatatype.NSEC and \
            response.authority[3].rdtype == dns.rdatatype.RRSIG 
        message = dns.message.make_query("hello." + domain, dns.rdatatype.LOC, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0 and \
            len(response.authority) == 4 and \
            response.authority[0].rdtype == dns.rdatatype.SOA and \
            response.authority[1].rdtype == dns.rdatatype.RRSIG and \
            response.authority[2].rdtype == dns.rdatatype.NSEC and \
            response.authority[3].rdtype == dns.rdatatype.RRSIG 

def test_nsec_nxdomain():
    for config in dnssec_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("doesnotexist." + domain, dns.rdatatype.A, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NXDOMAIN and len(response.answer) == 0 and \
            len(response.authority) == 6 and \
            response.authority[0].rdtype == dns.rdatatype.SOA and \
            response.authority[1].rdtype == dns.rdatatype.RRSIG and \
            response.authority[2].rdtype == dns.rdatatype.NSEC and \
            response.authority[3].rdtype == dns.rdatatype.RRSIG and \
            response.authority[4].rdtype == dns.rdatatype.NSEC and \
            response.authority[5].rdtype == dns.rdatatype.RRSIG 
        message = dns.message.make_query("does.not.exist." + domain, dns.rdatatype.A, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NXDOMAIN and len(response.answer) == 0 and \
            len(response.authority) == 6 and \
            response.authority[0].rdtype == dns.rdatatype.SOA and \
            response.authority[1].rdtype == dns.rdatatype.RRSIG and \
            response.authority[2].rdtype == dns.rdatatype.NSEC and \
            response.authority[3].rdtype == dns.rdatatype.RRSIG and \
            response.authority[4].rdtype == dns.rdatatype.NSEC and \
            response.authority[5].rdtype == dns.rdatatype.RRSIG 

def test_refused():
    for config in dnssec_configs:
        domain = "foobar.doesnotexist"
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.NS, want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.REFUSED and len(response.answer) == 0

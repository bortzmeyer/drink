defmodule DrinkResponsesConnectionTest do
  use ExUnit.Case
  doctest Drink.Responses.Connection
  test "txt" do
    %{data: s, rcode: r} = Drink.Responses.Connection.response(:txt, :tcp, {192, 0, 2, 128}, 12345)
    assert r == 0 and is_list(s) and Enum.at(s, 0) == "192.0.2.128:12345 over TCP"
  end
  test "srv" do
    assert %{rcode: 0} = Drink.Responses.Connection.response(:srv, :udp, {192, 0, 2, 254}, 53)
  end
end


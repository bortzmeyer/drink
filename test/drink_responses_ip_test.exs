defmodule DrinkResponsesIpTest do
  use ExUnit.Case
  doctest Drink.Responses.Ip
  test "a" do
    assert %{data: {192, 0, 2, 1}, rcode: 0} =
      Drink.Responses.Ip.response(:a, {192, 0, 2, 1})
  end
  test "aaaa" do
    addr = {0x2001, 0xdb8, 1, 0, 0, 0, 0, 0}
    assert %{data: ^addr, rcode: 0} =
      Drink.Responses.Ip.response(:aaaa, addr)
  end
  test "txt" do
    %{data: s, rcode: r} = Drink.Responses.Ip.response(:txt, {192, 0, 2, 128})
    assert r == 0 and is_list(s) and is_binary(Enum.at(s, 0))
  end
  test "srv" do
    assert %{rcode: 0} = Drink.Responses.Ip.response(:srv, {192, 0, 2, 254})
  end
end


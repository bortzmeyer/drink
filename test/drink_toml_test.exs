defmodule DrinkTomlTest do
  use ExUnit.Case
  doctest Drink.Toml
  
  test "parse1" do
    assert elem(Drink.Toml.load_config("example.toml", true), 0) == :ok
  end

  test "parse2" do
    assert elem(Drink.Toml.load_config("test/config1.toml", true), 0) == :ok
  end

  test "parse3" do
    result = Drink.Toml.load_config("test/config1.toml", true)
    case result do
      {:ok, config} -> assert config["port"] == 3553
      other -> assert False, "Got result #{inspect other}"
    end
  end

  test "parseerror1" do
    assert Drink.Toml.load_config("doesnotexist.toml", false) == {:error, :enoent}
  end

  test "parseerror2" do
    catch_exit Drink.Toml.load_config("test/invalid.toml", true)
  end

end

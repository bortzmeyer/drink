defmodule DrinkResponsesTest do
  use ExUnit.Case
  doctest Drink.Responses
  
  test "extract_addr" do
    v4 = {192, 0, 2, 1}
    v6 = {8193, 3512, 0, 0, 0, 0, 0, 1}
    assert Drink.Responses.includes_v4([]) == []
    assert Drink.Responses.includes_v4([v4, v6]) == [v4]
    assert Drink.Responses.includes_v6([v4, v6]) == [v6]
  end

  test "loc_valid" do
    # We just test it does not crash
    _result = Drink.Utils.loc(0, 0, 0)
    assert true
    _result = Drink.Utils.loc(10.0, -20.0, 0)
    assert true
    _result = Drink.Utils.loc(90.0, 180.0, 0)
    assert true
  end

  test "loc_invalid" do
    assert_raise Drink.LocError,
    fn -> 
      _result = Drink.Utils.loc(130.0, 0, 0)
    end
    assert_raise Drink.LocError,
    fn ->
      _result = Drink.Utils.loc(10.0, 200.0, 0)
    end
  end
  
end

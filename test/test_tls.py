from data import expectations, timeout, server, me, http_configs, dot_configs

import dns.message
import dns.query

import OpenSSL

import socket
import struct
import ssl

def one_question(sock, qname, qtype):
    request = dns.message.make_query(qname, qtype)
    data = request.to_wire()
    sock.send(struct.pack(">H", len(data)))
    sock.send(data)
    return request.id

def one_response(sock):
    length = struct.unpack(">H", sock.recv(2))[0]
    data = sock.recv(length)
    response = dns.message.from_wire(data)
    return response

def test_dot_one():
    for config in dot_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["dot-port"]
        context = ssl.create_default_context()
        context.set_alpn_protocols("dot")
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE  # Because the test certificate is self-signed
        sock = socket.create_connection((server, port))
        sock.settimeout(timeout)
        ssock = context.wrap_socket(sock,
                                    server_hostname=server)
        one_question(ssock, domain, dns.rdatatype.TXT)
        response = one_response(ssock)
        assert response.rcode() == dns.rcode.NOERROR and \
            "Possible queries: " in str(response.answer[0][0])
        ssock.shutdown(socket.SHUT_WR)
        
def test_dot_tls_values():
    for config in dot_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["dot-port"]
        context = ssl.create_default_context()
        context.set_alpn_protocols("dot")
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE  # Because the test certificate is self-signed
        sock = socket.create_connection((server, port))
        sock.settimeout(timeout)
        ssock = context.wrap_socket(sock,
                                    server_hostname=server)
        assert ssock.version() == "TLSv1.3"
        assert ssock.cipher()[0] == "TLS_AES_256_GCM_SHA384" # This test is too fragile, new ciphers may appear and be used?
        ssock.shutdown(socket.SHUT_WR)

def test_dot_certificate():
    for config in dot_configs:
        port = expectations[config]["dot-port"]
        subject_name = expectations[config]["subject"]
        cert_pem = ssl.get_server_certificate((server, port)) # We cannot
        # use getpeercert(), it returns an empty dictionary for
        # self-signed certificates.
        cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert_pem)
        assert cert.get_issuer().commonName == subject_name and cert.get_subject().commonName == subject_name
        
def test_dot_pipeline():
    for config in dot_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["dot-port"]
        if config in http_configs:
            questions = [domain, "hello." + domain, "ecs." + domain]
        else:
            questions = [domain, "hello." + domain]
        context = ssl.create_default_context()
        context.set_alpn_protocols("dot")
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE  # Because the test certificate is self-signed
        sock = socket.create_connection((server, port))
        sock.settimeout(timeout)
        ssock = context.wrap_socket(sock,
                                    server_hostname=server)
        ids = {}
        for name in questions:
            ids[one_question(ssock, name, dns.rdatatype.TXT)] = name
        for i in range(len(questions)):
            response = one_response(ssock)
            if response.id in ids: # We must use IDs to desambiguate
                # out-of-order replies (RFC 7766, section
                # 7)
                text = ""
                for s in response.answer[0][0].strings:
                    text += "%s " % s.decode()
                assert ids[response.id] + "." == str(response.question[0].name)
                del ids[response.id]
            else:
                assert False, "Unexpected ID %s" % response.id
        ssock.shutdown(socket.SHUT_WR)


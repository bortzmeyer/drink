from data import expectations, timeout, server, me, http_configs
from utils import address_family

import dns.message
import dns.query

def test_txt():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("connection." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        s = str(response.answer[0][0])
        assert response.rcode() == dns.rcode.NOERROR and \
            (me is None or me in s) and s.endswith("UDP\"")


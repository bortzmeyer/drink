import subprocess
import os

import pytest

from data import run_timeout

if "DRINK_PROD" not in os.environ and "DRINK_PROD2" not in os.environ:

    def test_incorrect():
        p = subprocess.run(["mix", "run", "drink.exs", "--port", "8053", "--config-file", "test/wrong.toml"], timeout=run_timeout, shell=False)
        assert p.returncode == 1
    

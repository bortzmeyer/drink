from data import expectations, timeout, server, me, http_configs
from utils import address_family

import dns.message
import dns.query

def test_txt():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("ip." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            (me is None or me in str(response.answer[0][0]))

def test_addr():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        if address_family(server) == 6:
            main = dns.rdatatype.AAAA
            secondary =  dns.rdatatype.A
        else:
            main = dns.rdatatype.A
            secondary =  dns.rdatatype.AAAA
        message = dns.message.make_query("ip." + domain, main)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        print(response)
        assert response.rcode() == dns.rcode.NOERROR and \
            (me is None or response.answer[0][0].address == me)
        message = dns.message.make_query("ip." + domain, secondary)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0 and \
            (me is None or response.additional[0][0].address == me)


defmodule DrinkEncodingTest do
  use ExUnit.Case
  doctest Drink.Encoding

  require Drink.RRTypes
  
  # Binaries are encoded as themselves
  test "binary1" do
    assert Drink.Encoding.encode(<<3, 4, 5>>) == <<3, 4, 5>>
  end

  test "address1" do
    resource =  %DNS.Resource{type: :a, domain: "foo.bar", class: :in, ttl: 30, data: {1, 2, 3, 4}} 
    assert Drink.Encoding.encode(resource) == <<3, ?f, ?o, ?o, 3, ?b, ?a, ?r, 0, 1::unsigned-integer-size(16), 1::unsigned-integer-size(16), 30::unsigned-integer-size(32), 4::unsigned-integer-size(16), 1, 2, 3, 4>>
  end

  test "string1" do
    assert Drink.Encoding.encode_string("chocolat") == <<8, ?c, ?h, ?o, ?c, ?o, ?l, ?a, ?t>>
  end

  test "txt1" do
    resource =  %DNS.Resource{type: :txt, domain: "foo.bar", class: :in, ttl: 30, data: ["chocolat"]}	
    assert Drink.Encoding.encode(resource) == <<3, ?f, ?o, ?o, 3, ?b, ?a, ?r, 0, 16::unsigned-integer-size(16), 1::unsigned-integer-size(16), 30::unsigned-integer-size(32), 9::unsigned-integer-size(16), 8, ?c, ?h, ?o, ?c, ?o, ?l, ?a, ?t>>
  end

  test "txt2" do
    resource =  %DNS.Resource{type: :txt, domain: "foo.bar", class: :in, ttl: 30, data: ["lait", "vin"]}	
    assert Drink.Encoding.encode(resource) == <<3, ?f, ?o, ?o, 3, ?b, ?a, ?r, 0, 16::unsigned-integer-size(16), 1::unsigned-integer-size(16), 30::unsigned-integer-size(32), 9::unsigned-integer-size(16), 4, ?l, ?a, ?i, ?t, 3, ?v, ?i, ?n>>
  end

  test "wallet1" do
    resource =  %DNS.Resource{type: Drink.RRTypes.wallet, domain: "foo.bar", class: :in, ttl: 30, data: ["BTC", "0"]}	
    assert Drink.Encoding.encode(resource) == <<3, ?f, ?o, ?o, 3, ?b, ?a, ?r, 0, 262::unsigned-integer-size(16), 1::unsigned-integer-size(16), 30::unsigned-integer-size(32), 6::unsigned-integer-size(16), 3, ?B, ?T, ?C, 1, ?0>>
  end

  # With Unicode. There is no standard for text charset/encoding in
  # DNS text strings. We decide to use Unicode/UTF-8 but we cannot
  # guarantee all client will handle it (dig, for instance, does not
  # display it properly).
  test "string2" do
    assert Drink.Encoding.encode_string("café") == <<5, 0x63, 0x61, 0x66, 0xc3, 0xa9>>
  end
  
end

from data import expectations, timeout, server, dnssec_configs, number_configs

import subprocess
import os

DIRPREFIX = "dnssec-tests"

if "DRINK_DOCKER" not in os.environ: # No DNSSEC yet in Docker

    def command(subdomain, domain, qtype, key=None, port=None):
        if subdomain != "" and not subdomain.endswith("."):
            subdomain = subdomain + "."
        if "DRINK_PROD" in os.environ or "DRINK_PROD2" in os.environ:
            return ["drill", "-S", subdomain + domain, qtype]
        else:
            return ["drill", "-S", "-k", "%s/%s.key" % (DIRPREFIX, key),
                    "-p", str(port), subdomain + domain,  "@%s" % server, qtype]
        
    def test_basic():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (DIRPREFIX, config)
            out = open(fname, "w")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            print("Testing %s with key %s" % (domain, key), file=out)
            print("", file=out)
            out.flush()
            # See dnssec-tests/README.md
            if "DRINK_PROD" in os.environ or "DRINK_PROD2" in os.environ:
                thecommand = command("", domain, "TXT")
            else:
                thecommand = command("", domain, "TXT", key, port)
            p = subprocess.Popen(thecommand,
                                         shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_nodata():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (DIRPREFIX, config)
            out = open(fname, "a")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            if "DRINK_PROD" in os.environ or "DRINK_PROD2" in os.environ:
                thecommand = command("", domain, "SRV")
            else:
                thecommand = command("", domain, "SRV", key, port)
            p = subprocess.Popen(thecommand,
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_nodomain():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (DIRPREFIX, config)
            out = open(fname, "a")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            if "DRINK_PROD" in os.environ or "DRINK_PROD2" in os.environ:
                thecommand = command("doesnotexist", domain, "TXT")
            else:
                thecommand = command("doesnotexist", domain, "TXT", key, port)
            p = subprocess.Popen(thecommand,
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_nodomain_more_labels():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (DIRPREFIX, config)
            out = open(fname, "a")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            if "DRINK_PROD" in os.environ or "DRINK_PROD2" in os.environ:
                thecommand = command("does.not.exist", domain, "TXT")
            else:
                thecommand = command("does.not.exist", domain, "TXT", key, port)
            p = subprocess.Popen(thecommand,
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_number_unicode():
        # In some locales like DE (german), the answer in the number
        # service is not in pure ASCII, and may create DNSSEC issue if
        # we don't sign properly (sign the bytes, not the charlist)..
        for config in number_configs:
            fname = "%s/drink-drill-%s.out" % (DIRPREFIX, config)
            out = open(fname, "w")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            print("Testing %s with key %s" % (domain, key), file=out)
            print("", file=out)
            out.flush()
            # See dnssec-tests/README.md
            if "DRINK_PROD" in os.environ or "DRINK_PROD2" in os.environ:
                thecommand = command("365.de.number", domain, "TXT")
            else:
                thecommand = command("365.de.number", domain, "TXT", key, port)
            p = subprocess.Popen(thecommand,
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        

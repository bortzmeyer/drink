import pytest

import os
import io
import tempfile
import subprocess
import time

from data import expectations

startup_delay = 0.5 # Increment between tests that the server actually started. In seconds.
maximum_trials = 30 # Maximum time to wait for the server is the product of these two parameters.

@pytest.fixture(autouse=True, scope="session")
def envtest():
    os.chdir("..")
    if "DRINK_PROD" not in os.environ and "DRINK_PROD2" not in os.environ and "DRINK_DOCKER" not in os.environ: 
        dir = tempfile.mkdtemp()
        processes = []
        files = []
        filenames = []
        for config in expectations:
            fname = "%s/drink-%s.txt" % (dir, config)
            out = open(fname, "w")
            files.append(out)
            filenames.append(fname)
            if "environ" in expectations[config]:
                env = expectations[config]["environ"]
            else:
                env = os.environ.copy()
            env["MIX_ENV"] = "test"
            p = subprocess.Popen(["mix", "run", "drink.exs", "--config-file", "test/%s.toml" % config,
                                  "--console", "--logging-level=debug"], shell=False,
                                 stdout=out, stderr=subprocess.STDOUT, env=env)
            processes.append(p)
        for name in filenames:
            f = open(name, "r")
            started = False
            trials = 0
            while not started:
                line = f.readline()
                if not line:
                    trials += 1
                    if trials > maximum_trials:
                        raise Exception("Too much attemps reading %s for the startup message. Drink server crashed?" % name)
                    time.sleep(startup_delay)
                elif "Receiving requests on port" in line:
                    started = True
                else:
                    pass  # Ignore other lines
            f.close()
    try:
        p = subprocess.Popen(["drill"], shell=False)
        p.kill()
        # continue
    except FileNotFoundError:
        assert False, "Command \"drill\" not found, please install it (package ldnsutils on Debian)"
    yield None
    if  "DRINK_PROD" not in os.environ and "DRINK_PROD2" not in os.environ and "DRINK_DOCKER" not in os.environ: 
        for p in processes:
            p.kill()
        for f in files:
            f.close()
        print("May be more details in the server(s) log files in \"%s\"" % dir)
    

defmodule DrinkDnssecTest do
  use ExUnit.Case
  doctest Drink.Dnssec
  
  test "bitmap1" do 
    assert Drink.Dnssec.nsec_bitmap([]) == <<>> # Empty NSEC allowed?
  end

  test "bitmap2" do # From a real example with an AMSL server. Note
		    # that we do not sort. We use data from the pcap.
    assert Drink.Dnssec.nsec_bitmap([46, 28, 1, 47]) == <<0, 6, 64, 0, 0, 8, 0, 3>>
  end

  test "bitmap3" do # This one exercices padding
    assert Drink.Dnssec.nsec_bitmap([1, 16]) == <<0, 3, 64, 0, 128>>
  end
  
  test "bitmap4" do # This one exercices a type which is near a bock frontier
    assert Drink.Dnssec.nsec_bitmap([1, 256]) == <<0, 1, 64, 1, 1, 128>>
  end
  
  test "bits1" do
    assert Drink.Dnssec.bits_of([1], 0) == [0, 1]
  end
      
  test "bits2" do
    assert Drink.Dnssec.bits_of([1, 15], 0) == [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
  end

  test "rfc1" do # Example of RFC 4034, section 4.3
    assert Drink.Dnssec.nsec_bitmap([1, 15, 46, 47, 1234]) == <<0, 6, 64, 1, 0, 0, 0, 3, 4, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32>>
  end
 
  test "next1" do
    d = DomainName.new!("foo.test")
    if Drink.Dnssec.rfc4470epsilon? do
      assert String.slice(DomainName.name(Drink.Dnssec.next_name(d)), 0, 5) == "\x00.foo"
    else
      assert String.slice(DomainName.name(Drink.Dnssec.next_name(d)), 0, 5) == "foo!."
    end
  end
  
  test "prev1" do
    d = DomainName.new!("foo.test")
    if Drink.Dnssec.rfc4470epsilon? do
      assert String.slice(DomainName.name(Drink.Dnssec.previous_name(d)), 0..2) == "fon"
    else
      assert String.slice(DomainName.name(Drink.Dnssec.previous_name(d)), 0..5) == "~.fon~"
    end
  end
  
  test "prev2" do
    d = DomainName.new!("f.test")
    p = DomainName.name(Drink.Dnssec.previous_name(d))
    if Drink.Dnssec.rfc4470epsilon? do
      assert String.slice(p, 0..0) == "e"
    else
      assert String.slice(p, 0..2) == "~.e"
    end
  end
  
  test "prev3" do
    d = DomainName.new!("*.test")
    prev = DomainName.name(Drink.Dnssec.previous_name(d))
    if Drink.Dnssec.rfc4470epsilon? do
      assert String.slice(prev, 0..1) == ")~"
    else
      assert String.slice(prev, 0..1) == "!~"
    end
  end

end

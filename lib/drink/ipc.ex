defmodule Drink.Ipc do
  use Agent # https://hexdocs.pm/elixir/Agent.html
  import Drink.Config
  
  require Logger
 
  def start_link(path) do
    Agent.start_link(fn -> path end, name: __MODULE__)
    spawn_monitor(Drink.Ipc, :accept, [])
  end

  def path do
    Agent.get(__MODULE__, & &1)
  end

 def accept() do
    {:ok, s} = :socket.open(:local, :stream, %{})
    File.rm(path()) # We don't test the result, we just want the file
		    # gone (may be it did not exist)
    result = :socket.bind(s, %{family: :local, path: path()})
    case result do
      :ok -> nil
      {:error, reason} ->
	Logger.error("IPC bind failed: #{inspect reason}")
	raise "Cannot bind to IPC socket #{path()}"
    end
    File.chmod!(path(), 0o660) # Owner and Group can read and write
    result = :socket.listen(s)
    case result do
      :ok -> Logger.debug("IPC started on #{path()}")
      {:error, reason} ->
	Logger.error("IPC listen failed: #{inspect reason}") 
	raise "Cannot listen on IPC socket #{path()}"
    end
    loop_acceptor(s) 
 rescue
    _e in ErlangError -> Logger.error("Cannot use the IPC socket, may be your Elixir version is too old (we need at least version 12)")
  end

  def loop_acceptor(socket) do
   {:ok, client} = :socket.accept(socket)
   Logger.debug("IPC serving #{inspect client}")
   spawn_monitor(Drink.Ipc, :serve, [client]) # We don't care what happens to the child process
   loop_acceptor(socket)
  end
  
  def serve(socket) do
    {result, command} = read_line(socket) 
    case result do
      :ok ->
	command = String.trim(command)
	Logger.info("IPC received from #{path()} \"#{command}\"")
	case command do
	  "help" -> write_line("Available commands:\r\nhelp\r\nstatus\r\nversion\r\nstatistics\r\nflush_statistics\r\nreport", socket)
	  "status" -> write_line("Drink is running", socket)
	  "version" -> write_line("Drink " <> List.to_string(Application.spec(:drink, :vsn)) <>
                         " DNS library version " <> List.to_string(Application.spec(:dns, :vsn)) <>
		         " Elixir version " <> System.version(), socket)
          "statistics" ->
	    if config()["statistics"] do
	      write_line(Drink.Statistics.get(), socket)
	    else
	      write_line("Statistics service not enabled", socket)
	    end
          "flush_statistics" ->
	    if config()["statistics"] do
	      write_line(Drink.Statistics.get(), socket)
	      write_line(Drink.Statistics.erase(), socket)
	    else
	      write_line("Statistics service not enabled", socket)
	    end
	  "report" ->
	    if config()["services"]["report"] do
	      data = Drink.Reports.get()
	      if length(data) > 0 do
		write_line("REPORT state:", socket)
		Enum.each(data, fn l -> write_line(l, socket) end)
	      else
		write_line("Nothing to report", socket)
	      end
	    else
	      write_line("DNS error reporting service not enabled", socket)
	    end
	  _ -> 	write_line("Unknown command \"#{command}\"", socket)
	end
	serve(socket)
      :error -> :socket.close(socket)
    end
  end
  
  def read_line(socket) do
    :socket.recv(socket, 0)
  end
  
  def write_line(line, socket) do
    :socket.send(socket, line <> "\r\n")
  end
  
end


defmodule Drink.Cli do

  import Drink.Defaults
  import Drink.Utils
  import Drink.Config
  
  @config_env_var "DRINK_CONFIG"

  @spec usage() :: no_return
  @spec usage(charlist) :: no_return
  @spec usage(charlist, boolean) :: no_return
  def usage(msg \\ nil, fatal \\ true) do
    IO.puts(:stderr, "Usage: drink [--port number] [--base domain-name] [--nameserver domain-name] [--maintainer email-address] [--bind ip-address] [--logging-level string] [--no-console]")
    if msg != nil do
      IO.puts(:stderr, msg) 
    end
    if fatal do
      exit("Wrong usage")
    else
      exit(:ok)
    end
  end

  @spec run({:ok | :error, :any}, [String.t()]) :: no_return
  def run({:ok, parsed}, argv) do
    result =
      OptionParser.parse(argv,
	strict: [help: :boolean, version: :boolean, config_file: :string,
		 port: :integer, bind: [:string, :keep], base: :string,
		 nameserver: :string, nsid: :string, maintainer: :string, 
		 logging_facility: :string, logging_level: :string,
		 negative_ttl: :integer, default_ttl: :integer, buffer_size: :integer,
		 console: :boolean, max_random: :integer,
		 statistics: :boolean,
		 secret_cookie: :string, ipc_socket: :string,
		 dnssec_key: :string, dnssec_key_tag: :integer,
		 weather_api_key: :string,
		 service: [:string, :keep], ipv4_only: :boolean, ipv6_only: :boolean,
		 dot_cert: :string, dot_key: :string, dot_port: :integer, padding: :boolean,
		 wallet: [:string, :keep],
		 # TODO allow to specify wallet on the
		 # command-line. Warning: two values requested
		 # (currency and address)
		 zoneversion: :boolean,
		 reporting_agent: :string, report_via_dns: :boolean,
	         # IETF experimental stuff:
	         greasing: :boolean],
	aliases: [h: :help, p: :port, b: :base, n: :nameserver, m: :maintainer, c: :console,
		  l: :logging_level, s: :services]
	# Unfortunately, OptionParser does not accept digits as
	# aliases so we cannot use -4 for --ipv4-only.
      )
    options =
      case result do
	{_, [_ | _], extra} -> usage("Extra remaining arguments after options: #{inspect extra}")
	{_, _, invalid} when invalid != [] -> usage("Invalid option(s): #{inspect(invalid)}")
	{opts, _, _} -> opts
      end
    help = Keyword.get(options, :help, false)
    if help do
      usage("See the README.md in the source distribution for details.", false)
    end
    version = Keyword.get(options, :version, false)
    if version do
      IO.puts("Drink #{inspect Application.spec(:drink, :vsn)}\ndns library version #{inspect Application.spec(:dns, :vsn)}\nElixir #{inspect System.build_info()}")
      exit(:ok)
    end
    config_file = Keyword.get(options, :config_file, nil)
    config_file =
    if config_file == nil do
      e = System.get_env(@config_env_var)
      if e == nil do
	nil
      else
	e
      end
    else
      config_file
    end
    parsed_config =
    if config_file == nil do
      parsed
    else
      {:ok, config} = Drink.Toml.load_config(config_file, true)
      config
    end
    config = Map.merge(default_config(), parsed_config)
    # Configuration file is flat. If we move to an tree one, it could
    # be a good idea to add the argument 'fn _key, left, right ->
    # Map.merge(left, right) end' to merge recursively. (It is more
    # complicated if we have both subtrees and final leafs, which
    # cannot be Map.merged.)

    cli_base = Keyword.get(options, :base, nil)
    bases =
    if cli_base != nil do
      [cli_base]
    else
      if is_list(config["base"]) do
	config["base"]
      else
	[config["base"]]
      end
    end
    bases = Enum.map(bases, fn b -> DomainName.new!(b) end)
    nserver = Keyword.get(options, :nameserver, config["nameserver"])
    t = String.split(nserver, "=")
    {nserver, addresses} =
    if length(t) == 1 do
      {Enum.at(t, 0), []}
    else
      {Enum.at(t, 0), Enum.drop(t, 1)}
    end
    nserver = DomainName.new!(nserver, [must_be_hostname: true])
    result = DomainName.ends_with(nserver, bases)
    {nserver_in_zone, ns_base} =
      case result do
	{true, ns_base} -> {true, ns_base}
	false -> {false, nil}
      end
    if not nserver_in_zone and addresses != [] do
      raise ArgumentError, "IP addresses mentioned for #{nserver} but the server is outside of the zone(s) #{inspect bases}"
    end
    if nserver_in_zone and addresses == [] do
      raise ArgumentError, "No IP addresses mentioned for #{nserver} but the server is inside of the zone(s) #{inspect bases}"
    end    
    # The rest will not be used if (not nserver_in_zone)
    nserver_sub =
    if nserver_in_zone do
      {:ok, short} = DomainName.without_suffix(nserver, ns_base)
      short
    else
      nserver
    end
    nserver_addr =
    Enum.map(addresses, fn a ->
	case :inet.parse_address(to_charlist(a)) do
	  {:ok, bin} -> bin
	  {_, _error} -> raise ArgumentError, "Invalid IP address \"#{a}\""
	end
    end)
    previous_nsid =
    if is_nil(config["nsid"]) do
      DomainName.name(nserver)
    else
      config["nsid"]
    end
    nsid = Keyword.get(options, :nsid, previous_nsid)
    maintainer = Keyword.get(options, :maintainer, config["maintainer"])
    maintainer = DomainName.new!(maintainer)
    port = Keyword.get(options, :port, config["port"])
    bind = Keyword.get_values(options, :bind)
    bind = if bind == [] do
      config["bind"]
    else
      bind
    end
    bbind = Enum.map(bind,
      fn addr ->
	if addr != :any do
	  case :inet.parse_address(to_charlist(addr)) do
	    {:ok, bin} -> bin
	    {_, error} -> usage("Invalid IP address \"#{addr}\": #{error}")
	  end
	else
	  addr
	end
      end)
    cookie_salt = Keyword.get(options, :secret_cookie, config["secret-salt-for-cookies"])	
    s_log_level = Keyword.get(options, :logging_level, config["logging-level"])
    log_level = str_to_loglevel(s_log_level)
    if log_level == nil do
      usage("Invalid logging level \"#{s_log_level}\"")
    end
    s_log_facility = Keyword.get(options, :logging_facility, config["logging-facility"])
    log_facility = str_to_logfacility(s_log_facility)
    if log_facility == nil do
      usage("Invalid logging facility \"#{s_log_facility}\"")
    end      
    ipc_socket = Keyword.get(options, :ipc_socket, config["ipc-socket"])	
    log_console = Keyword.get(options, :console, config["console"])
    statistics = Keyword.get(options, :statistics, config["statistics"])
    cli_dnssec_key_filename = Keyword.get(options, :dnssec_key, config["dnssec-key"])
    cli_dnssec_key_tag = Keyword.get(options, :dnssec_key_tag, config["dnssec-key-tag"])
    dnssec = (cli_dnssec_key_filename != nil)
    if dnssec and cli_dnssec_key_tag == nil do
      usage("For DNSSEC, you must indicate the key tag")
    end
    dnssec_key_filenames =
    if dnssec do
      if not is_list(cli_dnssec_key_filename) do
	[cli_dnssec_key_filename]
      else
	cli_dnssec_key_filename
      end
    else
      [nil]
    end
    if dnssec and length(dnssec_key_filenames) != length(bases) do
      usage("The length of the DNSSEC key list MUST be the same as the list of the base domain list (you may use nil for some domains)")
    end
    dnssec_key_tags =
    if dnssec do
      if not is_list(cli_dnssec_key_tag) do
	[cli_dnssec_key_tag]
      else
	cli_dnssec_key_tag
      end
    else
      [0]
    end
    if dnssec and length(dnssec_key_tags) != length(bases) do
      usage("The length of the DNSSEC key tag list MUST be the same as the list of the base domain list (you may use nil for some domains)")
    end
    bases_names = Enum.map(bases, &DomainName.name/1)
    domain_to_key = Enum.zip(bases_names, dnssec_key_filenames) |> Enum.into(%{})
    domain_to_tag = Enum.zip(bases_names, dnssec_key_tags) |> Enum.into(%{})
    dnssec_data = Map.new(Enum.filter(bases_names, fn domain -> domain_to_key[domain] != nil and domain_to_key[domain] != "" end),
      fn domain ->
	{rsa_priv_key, key_tag} =
	  {ExPublicKey.load!(cli_dnssec_key_filename), domain_to_tag[domain]}
	{:ok, rsa_pub_key} = ExPublicKey.public_key_from_private_key(rsa_priv_key)
	{domain,
	 %{
	   "file" => domain_to_key[domain],
	   "private-key" => rsa_priv_key,
           "public-key" => rsa_pub_key,
	   "key-tag" => key_tag}
	}
      end
    )
    negative_ttl = Keyword.get(options, :negative_ttl, config["negative-ttl"])
    default_nonnull_ttl = Keyword.get(options, :default_ttl, config["default-nonnull-ttl"])
    my_bufsize = Keyword.get(options, :buffer_size, config["server-bufsize"])
    lservices = Keyword.get_values(options, :service)
    services =
    if lservices == [] do
       Map.new(config["services"], fn s -> {s, true} end)
    else
      Map.new(lservices, fn s -> {s, true} end)
    end
    if Map.keys(services) == [] do
      IO.puts(:stderr, "Warning: no services selected (check the configuration file and the use of the --services option")
    end
    unknown = Enum.filter(services, fn {s, _v} -> s not in Drink.Defaults.possible_services() end)
    if length(unknown) > 0 do
      usage("Unknown service(s): #{Enum.join(Enum.map(unknown, fn {s, _v} -> s end), " ")}")
    end
    ipv4_only = Keyword.get(options, :ipv4_only, config["ipv4-only"])
    ipv6_only = Keyword.get(options, :ipv6_only, config["ipv6-only"])
    if ipv4_only and ipv6_only do
      usage("IPv4-only or IPv6-only but not both")
    end
    dot_cert = Keyword.get(options, :dot_cert, config["dot-cert"])
    dot_key = Keyword.get(options, :dot_key, config["dot-key"])
    dot_port = Keyword.get(options, :dot_port, config["dot-port"])
    if (dot_cert != nil and dot_key == nil) or (dot_cert == nil and dot_key != nil) do
      usage("If you want to enable DoT, you need to define both dot-cert and dot-key")
    end
    padding = Keyword.get(options, :padding, config["padding"])
    dot = dot_cert != nil and dot_key != nil 
    if dot do
      # Just check the files exist and are readable
      File.open!(dot_cert, [:read])
      File.open!(dot_key, [:read])
    end
    max_random = Keyword.get(options, :max_random, config["max-random-value"])
    geoip_api_key = config["geoip-api-key"]
    weather_api_key = Keyword.get(options, :weather_api_key, config["weather-api-key"])
    if services["weather"] != nil and weather_api_key == nil do
      usage("For the weather service, we need an API key")
    end
    wallet = Keyword.get(options, :wallet, config["wallet"])
    # Now, IETF experimental stuff
    reporting_agent = Keyword.get(options, :reporting_agent,
      config["reporting-agent"])
    reporting_agent =
    if reporting_agent == nil do
      nil
    else
      DomainName.new!(reporting_agent)
    end
    report_via_dns = Keyword.get(options, :report_via_dns, config["report-via-dns"])
    zoneversion = Keyword.get(options, :zoneversion, config["zoneversion"])
    greasing = Keyword.get(options, :greasing, config["greasing"])
    Map.put(config, "bases", bases)
    |> Map.put("port", port)
    |> Map.put("negative-ttl", negative_ttl)
    |> Map.put("default-nonnull-ttl", default_nonnull_ttl)
    |> Map.put("nserver", nserver)
    |> Map.put("nserver-in-zone", nserver_in_zone)
    |> Map.put("nserver-short", DomainName.labels(nserver_sub))
    |> Map.put("nserver-addresses", nserver_addr)
    |> Map.put("nsid", nsid)
    |> Map.put("dnssec", dnssec)
    |> Map.put("dnssec-data", dnssec_data)
    |> Map.put("maintainer", maintainer)
    |> Map.put("log-level", log_level)
    |> Map.put("log-facility", log_facility)
    |> Map.put("console", log_console)
    |> Map.put("statistics", statistics)
    |> Map.put("bind", bbind)
    |> Map.put("secret-salt-for-cookies", cookie_salt)
    |> Map.put("ipc-socket", ipc_socket)
    |> Map.put("services", services)
    |> Map.put("ipv4-only", ipv4_only)
    |> Map.put("ipv6-only", ipv6_only)
    |> Map.put("dot-cert", dot_cert)
    |> Map.put("dot-key", dot_key)
    |> Map.put("dot-port", dot_port)
    |> Map.put("dot", dot)
    |> Map.put("padding", padding)
    |> Map.put("server-bufsize", my_bufsize)
    |> Map.put("max-rand-value", max_random)
    |> Map.put("geoip-api-key", geoip_api_key)
    |> Map.put("weather-api-key", weather_api_key)
    |> Map.put("wallet", wallet)
    # IETF experimentation for Internet drafts
    |> Map.put("reporting-agent", reporting_agent)
    |> Map.put("report-via-dns", report_via_dns)
    |> Map.put("zoneversion", zoneversion)
    |> Map.put("greasing", greasing)
    # Now, we really start
    |> Map.put("server-start", DateTime.utc_now())
  end

  def run({:error, reason}, _argv) do
    exit(inspect reason)
  end

  @spec main([String.t()]) :: no_return
  def main(argv) do
    Drink.Toml.load_configs
    |> Drink.Cli.run(argv)
    |> Drink.Config.start_link
    
    if config()["services"]["report"] do
      Drink.Reports.start_link
    end
    if config()["ipc-socket"] do
      Drink.Ipc.start_link(config()["ipc-socket"])
    end
    if config()["statistics"] do
      Drink.Statistics.start_link()
    end
    Drink.Server.start()
  end
    
end



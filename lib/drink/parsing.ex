defmodule Drink.Parsing do

  require Logger

  @type dns_request  :: %{
    required(:qname) => String.t(),
    required(:qtype) => atom | integer,
    required(:header) => nil,
    required(:request) => nil,
    required(:version) => nil | atom | integer, # EDNS version
    optional(:dnssec_ok) => boolean,
    optional(:size) => integer, # EDNS buffer size
    optional(:edns) => Drink.Edns.edns}

  @spec parse_request(binary) :: {:error, atom} | {:problem, atom, dns_request} | {:ok, dns_request}
  def parse_request(data, log \\ true) do
    case DNS.Record.decode(data) do
      %DNS.Record{header: header, qdlist: request, arlist: additional} ->
	if header.opcode != :query or header.qr do
	  {:error, :notQuery}
	else
	  edns =
	    try do
	      Drink.Edns.parse_additional(additional)
	    rescue 
	    e in Drink.EdnsError ->
	      if log do
		Logger.info("Wrong EDNS options: #{inspect e}")
	      end
	      {:problem, :badEDNSopt}
	    end
	  r = Enum.at(request, 0) # We assume only one question. The DNS
	  # format allows several but it is underspecified and never
	  # seen in practice.
	  case edns do
	    {0, client_bufsize, edns, dnssec_ok, compact_ok} ->
	      {:ok, %{:qname => to_string(r.domain), :qtype => r.type,
		      :header => header, :request => request,
		      :version => 0, :size => client_bufsize,
		      :dnssec_ok => dnssec_ok,
		      :compact_ok => compact_ok,
		      :edns => edns}}
	    {nil} ->
	      {:ok, %{:qname => to_string(r.domain), :qtype => r.type,
		      :header => header, :request => request,
		      :version => nil, :size => 512,
		      :dnssec_ok => false,
		      :compact_ok => false,
		      :edns => %{:nsid => false, :ecs => nil, :cookies => nil}}} # 512
	              # is the default size of DNS for UDP, see RFC
	              # 1035, section 4.2.1. In theory, we could use a
	              # larger size for TCP but we don't.
	    {:problem, :badEDNSopt} ->
	      {:problem, :badEDNSopt, %{:qname => to_string(r.domain), :qtype => r.type,
					:header => header, :request => request, :version => nil}}
	    {_other_version} ->
	      {:ok, %{:qname => to_string(r.domain), :qtype => r.type,
		      :header => header, :request => request,
		      :version => :wrongEDNSversion}}
	  end
	end
      # Other cases never occur, decode raises an exception instead
	end
  rescue
    e -> # decode can raise an exception
     if log do
	Logger.info("Wrong DNS message: #{inspect e}")
      end
      {:error, :badDNS}
  end
  
end

defmodule Drink.Statistics do
  use Agent # https://hexdocs.pm/elixir/Agent.html
  
  require Logger

  import Drink.Config
  
  @default %{protocols: %{}, rcodes: %{}, qtypes: %{}, qnames: %{}}
  
  def start_link() do
    Agent.start_link(fn -> @default end, name: __MODULE__)
  end

  def post(what, value) do
    Agent.update(__MODULE__,
      fn state ->
        {_old, p} = Map.get_and_update(state[what], value, fn v -> if v == nil do {0, 1} else {v, v+1} end end)
	Map.put(state, what, p)
      end)
  end

  def get do
    l = Agent.get(__MODULE__, & &1)
    now = DateTime.utc_now()
    running = DateTime.diff(now, config()["server-start"])
    erased =
    if l[:erasure] != nil do
      DateTime.diff(now, l.erasure)
    else
      nil
    end
    l
    |> Map.put("server-start", DateTime.to_iso8601(config()["server-start"]))
    |> Map.put("server-version", "Drink: #{Application.spec(:drink, :vsn)} DNS library: #{Application.spec(:dns, :vsn)} Elixir: #{System.version()}")
    |> Map.put("when", DateTime.to_iso8601(now))
    |> Map.put("running-seconds", running)
    |> Map.put("running-time", Drink.Utils.Convert.seconds_to_human(running))
    |> Map.put("erased-seconds", erased)
    |> Map.put("erased-time",
       if erased != nil do
         Drink.Utils.Convert.seconds_to_human(erased)
       else
         "Never"
       end)
    |> Jason.encode!
  end

  def erase do
    Agent.update(__MODULE__, fn _state -> @default end)
    Agent.update(__MODULE__, fn state -> Map.put(state, :erasure, DateTime.utc_now)  end)
    "Statistics erased" 
  end

end


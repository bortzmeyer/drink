defmodule Drink.Encoding do
  @moduledoc """

  Encode DNS resource records (RR).

  Yes, the Erlang :inet_dns module (and its Elixir interface "dns") can
  encode. But they have several limpitations, some serious, even
  blocking:
  * only a small set of RR types (for instance, LOC and DNSKEY are not handled),
  * no way to encode just a RR, you have to encode an entire message
    (which is useful for name compression but may be too rigid other
    cases),
  * no way to disable name compression, which is a no-no for DNSSEC,
    whose canonicalisation prohibits compression.
  """

  require Logger

  require Drink.RRTypes
  
  @in_class 1 # Class IN is hardwired, this is the only one we
		# support. https://datatracker.ietf.org/doc/draft-sullivan-dns-class-useless/

  @spec encode_string(binary) :: binary
  def encode_string(s) do
    bs = Binary.from_list(
    if is_list(s) do
      s
    else
     # We don't use to_charlist because we don't want the list of code
     # points, we want the bytes in UTF-8.
     Binary.to_list(s) 
    end)
    Binary.append(<<byte_size(bs)::unsigned-integer-size(8)>>, bs)
  end
  
  @spec encode(binary | [%DNS.Resource{}] | %DNS.Resource{} ) :: binary
  def encode(b) when is_binary(b) do
    # Already encoded
    b
  end
  def encode(rrset) when is_list(rrset) do
    # It would be safer to check that it is indeed a rrset (same domain name and type)
    Enum.reduce(rrset, <<>>, fn rr, acc -> Binary.append(acc, encode(rr)) end)
  end
  def encode(rr) do
    # We do not perform name compression because of DNSSEC. If you add
    # support for name compression (which will probably require the
    # ability to encode an entire message, to know all the domain
    # names), make it a deselectable option.
    case rr do
      %DNS.Resource{type: :a, domain: domain, class: :in, ttl: ttl, data: address} ->
	encoded = <<elem(address, 0)::unsigned-integer-size(8),elem(address, 1)::unsigned-integer-size(8),elem(address, 2)::unsigned-integer-size(8),elem(address, 3)::unsigned-integer-size(8)>>
	encode(domain, :a, ttl, encoded)
      %DNS.Resource{type: :aaaa, domain: domain, class: :in, ttl: ttl, data: address} ->
	encoded = <<elem(address, 0)::unsigned-integer-size(16),elem(address, 1)::unsigned-integer-size(16),elem(address, 2)::unsigned-integer-size(16),elem(address, 3)::unsigned-integer-size(16),elem(address, 4)::unsigned-integer-size(16),elem(address, 5)::unsigned-integer-size(16),elem(address, 6)::unsigned-integer-size(16),elem(address, 7)::unsigned-integer-size(16)>>
	encode(domain, :aaaa, ttl, encoded)
      %DNS.Resource{type: :ns, domain: domain, class: :in, ttl: ttl, data: ns} ->
	encoded = Binary.from_list(DomainName.new!(ns, [must_be_hostname: true]) |> DomainName.encode)
	encode(domain, :ns, ttl, encoded)
      %DNS.Resource{type: :soa, domain: domain, class: :in, ttl: ttl, data: values} ->
	mname = Binary.from_list(DomainName.new!(elem(values, 0), [must_be_hostname: true]) |> DomainName.encode)
	rname = Binary.from_list(DomainName.new!(elem(values, 1)) |> DomainName.encode)
	encoded = Binary.append(mname, Binary.append(rname, <<elem(values, 2)::unsigned-integer-size(32),elem(values, 3)::unsigned-integer-size(32),elem(values, 4)::unsigned-integer-size(32),elem(values, 5)::unsigned-integer-size(32),elem(values, 6)::unsigned-integer-size(32)>>))
	encode(domain, :soa, ttl, encoded)
      %DNS.Resource{type: :txt, domain: domain, class: :in, ttl: ttl, data: strings} ->
	encoded = Enum.reduce(strings, <<>>,
	fn s, acc ->
	  Binary.append(acc,
            encode_string(s)) end)
	encode(domain, :txt, ttl, encoded)
      %DNS.Resource{type: :mx, domain: domain, class: :in, ttl: ttl, data: values} ->
	encoded = Binary.append(<<elem(values, 0)::unsigned-integer-size(16)>>, Binary.from_list(DomainName.new!(elem(values, 1)) |> DomainName.encode))
	encode(domain, :mx, ttl, encoded)
      %DNS.Resource{type: :hinfo, domain: domain, class: :in, ttl: ttl, data: values} ->
	encoded = Binary.append(encode_string(elem(values, 0)), encode_string(elem(values, 1)))
	encode(domain, :hinfo, ttl, encoded)
      %DNS.Resource{type: :uri, domain: domain, class: :in, ttl: ttl, data: values} ->
	encoded = <<elem(values, 0)::unsigned-integer-size(16), elem(values, 1)::unsigned-integer-size(16) , elem(values, 2)::binary>>
	encode(domain, :uri, ttl, encoded)
      %DNS.Resource{type: unknown_type, domain: _domain, class: :in, ttl: _ttl, data: _values} when is_atom(unknown_type) ->
	Logger.error("Unknown RR type to encode: #{unknown_type}")
	<<>>
      %DNS.Resource{type: type, domain: domain, class: :in, ttl: ttl, data: data} when is_integer(type) ->
	if type == Drink.RRTypes.wallet do
	    encoded = Enum.reduce(data, <<>>,
	    fn s, acc ->
	      Binary.append(acc,
		encode_string(s)) end)
	    encode(domain, :wallet, ttl, encoded)
	else
	  # Already encoded
	  encode(domain, type, ttl, data)
	end
      _ ->
	raise ArgumentError, "Not a DNS resource record, or an unsupported one"
    end
  end

  def encode(domain, type, ttl, data) do
    Binary.append(Binary.append(Binary.from_list(DomainName.new!(domain) |> DomainName.encode), <<Drink.RRTypes.rrtype_to_num(type)::unsigned-integer-size(16), @in_class::unsigned-integer-size(16), ttl::unsigned-integer-size(32), byte_size(data)::unsigned-integer-size(16)>>), data)
  end
  
end

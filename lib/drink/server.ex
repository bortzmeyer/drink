defmodule Drink.Server do

  @moduledoc """ 
  Heart of the DNS server. It uses directly the Erlang libraries such
  as [gen_udp](https://www.erlang.org/doc/man/gen_udp.html) or
  [gen_tcp](https://www.erlang.org/doc/man/gen_tcp.html) since
  Elixir's [Socket library](https://hexdocs.pm/socket/) is [way too
  buggy](https://github.com/meh/elixir-socket/issues) and
  unmaintained.  We don't use Dns.Server module in the [dns
  library](https://github.com/tungd/elixir-dns/) because it is very
  limited (no [IPv6](https://github.com/tungd/elixir-dns/issues/43),
  no [TCP](https://github.com/tungd/elixir-dns/issues/45), no
  parallelism).
  """

  # https://hexdocs.pm/logger/
  require Logger

  import Drink.Utils
  import Drink.Config

  require Drink.EdnsCodes
  require Drink.RCodes
  require Drink.RRTypes
  
  @timeout 2000  # Milli-seconds
  @read_length 4096 # Should be enough for DNS over UDP
  
  @stats_delay 600000 # Milli-seconds

  @pad_block_size 468 # RFC 8467, section 4.1
  
  @minimum_ttl 0 # If it creates problems with some resolvers, please
		 # document it precisely. For instance, the A/AAAA
		 # trick of the "ip" service disturbs DNSviz which
		 # says "NSEC proving non-existence of
		 # ip.dyn.bortzmeyer.fr/A: The following queries
		 # resulted in an answer response, even though the
		 # bitmap in the NSEC RR indicates that the queried
		 # records don't exist: ip.dyn.bortzmeyer.fr/A",
		 # because DNSviz keeps the NSEC of a previous request
		 # (in the same analysis) even with a zero TTL.

  # Greasing parameters, currently not changeable draft-ietf-dnsop-grease
  @greasing_percentage 75 # If value=10, it will mean we will grease 10 % of the responses.
  @greasing_maximum_edns_version 9
  @greasing_max_num_options 4
  # Currently, the draft about greasing does does not reserve a range for option codes.
  @greasing_minimum_edns_option 10000
  @greasing_maximum_edns_option 20000
  @greasing_max_option_length 25
  @greasing_maximum_flag_value 1000
  
  if Mix.target() == :http or Mix.target() == :full do
    def start_http do
      HTTPoison.start() # Necessary for stuff like "bgp" requests.
      Application.put_env(:geoip, :provider, Drink.Defaults.geoip_provider)
      if config()["geoip-api-key"] != nil do
        Application.put_env(:geoip, :api_key, config()["geoip-api-key"])
      end			    
    end
  else
    def start_http do
    end
  end

  @spec start() :: no_return
  def start() do
    Logger.configure([level: config()["log-level"]])
    Logger.add_backend({LoggerSyslogBackend, :default_logger})
    Logger.configure_backend({LoggerSyslogBackend, :default_logger},
      [facility: config()["log-facility"], 
       app_id: "Drink",
       format: "[$level] $metadata $message",
       metadata: [:all]])
    if not config()["console"] do
      Logger.remove_backend(Logger.Backends.Console)
    end
    start_http()
    version =
    if config()["ipv4-only"] do
      :inet
    else 
      :inet6
      # Depending on the OS parameters (sysctl net.ipv6.bindv6only on
      # Linux), it allows us to listen on IPv6 *and* IPv4. 
    end
    addresses =
    case config()["bind"] do
      [:any] -> [:any]
      l ->
	Enum.filter(l,
	  fn address -> (config()["ipv4-only"] and address_family(address) == :inet4) or 
	    (config()["ipv6-only"] and address_family(address) == :inet6) or
	    (not config()["ipv4-only"] and not config()["ipv6-only"]) end)
    end
    _udp_servers =
      Enum.map(addresses, fn address ->
	Logger.info("Starting UDP server for #{a2s(address)}")
	options = [version, :binary, {:ip, address}, active: false]
	options =
	if config()["ipv6-only"] do
	  options ++ [ipv6_v6only: true] # Warning: if you do NOT want
					 # the ipv6_v6only option, do
					 # not use it, even with value
					 # "false".
	else
	  options
	end
	socket_result =
          :gen_udp.open(config()["port"], options)
	socket = socket_open(socket_result)
	udp_pid = spawn_link(Drink.Server, :udp_loop_acceptor,
	  [socket, config()["bases"]])
	Process.monitor(udp_pid)
      end)
    _tcp_servers =
      Enum.map(addresses, fn address ->
	Logger.info("Starting TCP server for #{a2s(address)}")
	options = [version, :binary, {:ip, address}, active: false,
		   packet: 2]  # Automatically add/read a 2-bytes
			       # length before data. See
			       # https://www.erlang.org/doc/man/inet.html#setopts-2
	options =
	if config()["ipv6-only"] do
	  options ++ [ipv6_v6only: true]
	else
	  options
	end
	socket_result = :gen_tcp.listen(config()["port"], options)
	socket = socket_open(socket_result)
	tcp_pid = spawn_link(Drink.Server, :tcp_loop_acceptor,
	  [socket, config()["bases"]])
	Process.monitor(tcp_pid)
      end)
    _tls_servers =
    if config()["dot"] do
      Enum.map(addresses, fn address ->
	Logger.info("Starting DoT server for #{a2s(address)}")
	options = [version,
	   :binary,
	   {:ip, address},
	   active: false,
	   packet: 2,
	   certfile: config()["dot-cert"],
	   keyfile:  config()["dot-key"]]
	   # advertised_protocols: ["dot"]])
	options =
	if config()["ipv6-only"] do
	  options ++ [ipv6_v6only: true]
	else
	  options
	end
	socket_result = :ssl.listen(config()["dot-port"], options)
	socket = socket_open(socket_result, config()["dot-port"])
	tls_pid = spawn_link(Drink.Server, :tls_loop_acceptor,
	  [socket, config()["bases"]])
	Process.monitor(tls_pid)
      end)
    else
      []
    end
    Logger.warn("Receiving requests on port #{config()["port"]}") # Logger.notice appeared only with Elixir 1.11
    stats_pid = spawn_link(Drink.Server, :stats_print, [])
    Process.register(stats_pid, Drink.Stats)
    # Block here forever while the servers (UDP and TCP) run
    receive do
      {:DOWN, ref, type, proc, reason} ->
	Logger.error("DNS server #{inspect type} #{inspect(proc)} (#{inspect(ref)}) ended, but it should never happen: #{inspect reason}")
    end
  end

  def stats() do
    "[VM resources] Processes: #{:erlang.system_info(:process_count)}/#{:erlang.system_info(:process_limit)} Ports: #{:erlang.system_info(:port_count)}/#{:erlang.system_info(:port_limit)} Atoms: #{:erlang.system_info(:atom_count)}/#{:erlang.system_info(:atom_limit)} Memory: #{:erlang.memory(:total)} bytes" # https://www.erlang.org/doc/man/erlang.html#system_info-1 https://www.erlang.org/doc/man/erlang#memory-1
  end
						    
  def stats_print() do
	Logger.info(stats())
	:timer.sleep(@stats_delay)
	stats_print()
  end
      
  def socket_open(result, port \\ config()["port"]) do
    case result do
      {:ok, socket} -> socket
      {:error, :eacces} ->
	Logger.error("Cannot bind to port #{port}: permission denied")
	exit("Permission denied")
      {:error, :eaddrinuse} ->
	Logger.error("Cannot bind to port #{port}: already in use")
	exit("Port already in use")
      {:error, reason} ->
	Logger.error("Cannot bind to port #{port}: #{inspect reason}")
	exit("Generic error")
    end
  end

  @spec udp_loop_acceptor(integer, charlist) :: no_return
  def udp_loop_acceptor(socket, bases) do
    read = :gen_udp.recv(socket, @read_length) 
    case read do
      {:ok, {client, port, request}} ->
	spawn(Drink.Server, :serve, [:udp, socket, bases, client, port, request])
      {:error, reason} ->
	Logger.error("Read error on UDP socket #{inspect socket}: #{inspect reason}")
    end
    udp_loop_acceptor(socket, bases)
  end

  @spec tcp_loop_acceptor(integer, charlist) :: no_return
  def tcp_loop_acceptor(socket, bases) do
    accepted = :gen_tcp.accept(socket)
    case accepted do
      {:ok, client_socket} ->
	remote_client = :inet.peername(client_socket)
	case remote_client do
	  {:ok, {client, port}} ->
	    Logger.debug("Starting a new TCP session with #{a2s client}")
	    spawn(Drink.Server, :tcp_request_acceptor, [client_socket, bases, client, port])
	  {:error, reason} ->
	    Logger.error("Cannot find remote client info: #{inspect reason}")
	end
      {:error, :system_limit} ->   
	Logger.error("Accept error TCP: a limit was reached.")
	Logger.error(stats()) # https://www.erlang.org/doc/efficiency_guide/advanced.html#system-limits
      {:error, reason} ->
	Logger.error("Accept error on TCP socket #{inspect socket}: #{inspect reason}")    end
    tcp_loop_acceptor(socket, bases)
  end

  @spec tls_loop_acceptor(integer, charlist) :: no_return
  def tls_loop_acceptor(socket, bases) do
    result = :ssl.transport_accept(socket)
    case result do
      {:ok, client} ->
	accepted = :ssl.handshake(client)
	case accepted do
	  {:ok, client_socket} ->
	    remote_client = :ssl.peername(client)
	    case remote_client do
	      {:ok, {client, port}} ->
		Logger.debug("Starting a new TCP session with #{inspect(a2s client)}")
		spawn(Drink.Server, :tls_request_acceptor, [client_socket, bases, client, port])
	      {:error, reason} ->
		Logger.error("Cannot find remote client info: #{inspect reason}")
	    end
	  {:error, reason} ->
	    Logger.error("Cannot handshake TLS session #{inspect reason}")
	end
      {:error, :system_limit} ->   
	Logger.error("Accept error TLS: a limit was reached.")
	Logger.error(stats()) # https://www.erlang.org/doc/efficiency_guide/advanced.html#system-limits
      {:error, reason} ->
	Logger.error("Accept error on TLS socket #{inspect socket}: #{inspect reason}")
    end
    tls_loop_acceptor(socket, bases)
  end

  def tcp_request_acceptor(socket, bases, client, port) do
    result = :gen_tcp.recv(socket, 0, @timeout)
    case result do 
      {:ok, nil} -> # No data yet
	tcp_request_acceptor(socket, bases, client, port)
      {:ok, request} ->
	spawn(Drink.Server, :serve, [:tcp, socket, bases, client, port, request]) # We
	# spawn a new process for each request, so, we may send
	# responses out-of-order (no problem, RFC 7766, section 7).
	tcp_request_acceptor(socket, bases, client, port) # Wait on the
	# same TCP connection, to reuse it (you can test with dig's
        # +keepopen option).
      {:error, :closed} -> # Nothing to do
        Logger.debug("Normal end of TCP #{inspect socket}")
	Port.close(socket)
      {:error, :timeout} ->
	Logger.error("Timeout on TCP socket #{inspect socket} from client #{a2s client}")
	:gen_tcp.shutdown(socket, :read_write)  
	Port.close(socket)
      {:error, :enotconn} ->
        Logger.debug("End of TCP #{inspect socket}")
        :gen_tcp.shutdown(socket, :read_write) # Probably useless since, if we are here, it means the socket is closed.
	Port.close(socket) # Reclaiming the Erlang (not TCP) port is important, otherwise we'll run in a
	# :system_limit problem (see issue #35).
      _ -> Logger.error("Unexpected read on TCP socket #{inspect socket} from client #{a2s client}: #{inspect result}")
    end
  end

  def tls_request_acceptor(socket, bases, client, port) do
    result = :ssl.recv(socket, 0, @timeout)
    case result do 
      {:ok, nil} -> # No data yet
	tls_request_acceptor(socket, bases, client, port)
      {:ok, request} ->
	spawn(Drink.Server, :serve, [:tls, socket, bases, client, port, request]) 
	tls_request_acceptor(socket, bases, client, port) 
      {:error, :closed} -> # Nothing to do
        Logger.debug("Normal end of TLS #{inspect socket}")
      {:error, :timeout} ->
	Logger.error("Timeout on TLS socket #{inspect socket} from client #{a2s client}")
	:ssl.shutdown(socket, :read_write)  
      {:error, :enotconn} ->
        Logger.debug("End of TLS #{inspect socket}")
        :ssl.shutdown(socket, :read_write) 
      _ -> Logger.error("Unexpected read on TLS socket #{inspect socket} from client #{a2s client}: #{inspect result}")
    end
  end

  def serve(protocol, socket, bases, client, port, data) do
    Logger.debug("Serving #{a2s client}")
    if config()["statistics"] do
      Drink.Statistics.post(:protocols, protocol)
    end
    Drink.Parsing.parse_request(data)
    |> make_response(protocol, bases, client, port)
    |> write(protocol, socket, client, port)
  end

  @type options :: %{
    optional(:nsid) => boolean,
    optional(:cookies) => Drink.Edns.cookie,
    optional(:zoneversion) => boolean,
    optional(:reporting) => String.t(),
    optional(:ede) => {integer, String.t()}
  }
  @spec opt_record(boolean | atom, options, integer, boolean) :: [%DNS.Resource{}]
  def opt_record(edns, options \\ %{}, size \\ config()["server-bufsize"], dnssec_ok \\ false)  
  def opt_record(true, opts, bufsize, dnssec_ok) do
    # Padding must be added at the end of the response construction so
    # we don't do it here.
    the_record =
    if opts[:nsid] do
      length = byte_size(config()["nsid"])
      Binary.append(<<Drink.EdnsCodes.nsid::unsigned-integer-size(16),
	length::unsigned-integer-size(16)>>, config()["nsid"])
    else
      <<>>
    end
    # This is for greasing draft-ietf-dnsop-grease-00
    greasing = config()["greasing"] and (Enum.random(0..100) < @greasing_percentage) 
    the_record = the_record <>
    if greasing do
      quantity = Enum.random(0..@greasing_max_num_options)
      if quantity > 0 do
	Enum.reduce(1..quantity,
	  <<>>,
	  fn _n, accu -> 
	    length = Enum.random(1..@greasing_max_option_length)
	    grease_option = Enum.random(@greasing_minimum_edns_option..@greasing_maximum_edns_option)
	    accu <> Binary.append(<<grease_option::unsigned-integer-size(16),
	      length::unsigned-integer-size(16)>>, :binary.copy(<<?X>>, length))
	  end)
      else
	<<>>
      end
    else
      <<>>
    end
    the_record = the_record <>
    # It could be interesting to have a knob to authorize something
    # like 'send only every N responses' (requires a state) or with a
    # probability of 1/N.
    if config()["reporting-agent"] != nil and DomainName.labels(config()["reporting-agent"]) != [] do
      report_to = Binary.from_list(DomainName.encode(config()["reporting-agent"]))
      length = byte_size(report_to)
      Binary.append(<<Drink.EdnsCodes.reporting::unsigned-integer-size(16),
	length::unsigned-integer-size(16)>>, report_to)
    else
      <<>>
    end
    the_record = the_record <>
    if config()["zoneversion"] and opts[:zoneversion] do
      opt_length = 6
      flag = 0 # SOA serial https://www.iana.org/assignments/dns-parameters/dns-parameters.xml#zoneversion-type-values
      basename = List.first(config()["bases"])
      labelcount = length(DomainName.labels(basename))
      zv = Drink.Utils.serial()
      <<Drink.EdnsCodes.zoneversion::unsigned-integer-size(16), opt_length::unsigned-integer-size(16),
       labelcount::unsigned-integer-size(8), flag::unsigned-integer-size(8), 
       zv::unsigned-integer-size(32)>>
    else
      <<>>
    end
    the_record = the_record <>
      case opts[:cookies] do
	nil -> <<>>
	{c, s} -> <<Drink.EdnsCodes.cookie::unsigned-integer-size(16), 16::unsigned-integer-size(16)>> <> c <> s   
      end
    the_record = the_record <>
      case opts[:ede] do
	nil -> <<>>
	{code, text} ->
	  length = 2 + byte_size(text)
          Binary.append(<<Drink.EdnsCodes.extended::unsigned-integer-size(16), length::unsigned-integer-size(16),
	    code::unsigned-integer-size(16)>>, text)
      end
    my_edns_version = 0
    # This is for greasing draft-ietf-dnsop-grease-00
    future_edns_version =
    if greasing do
      Enum.random(1..@greasing_maximum_edns_version) * round(:math.pow(2, 16))
    else
      my_edns_version
    end
    ttl = # See RFC 6891, section 6.1.3. Everything is zero, except
			       # may be the "DNSSEC OK" bit (32768 in
			       # decimal). Or if we grease.
    if dnssec_ok do
      32768 + my_edns_version
    else
      my_edns_version
    end
    ttl_new_version =
    if greasing do
      if dnssec_ok do
	32768 + future_edns_version
      else
	future_edns_version
      end
    else
      my_edns_version
    end
    my_ttl =
    if greasing do
      ttl_new_version + Enum.random(0..@greasing_maximum_flag_value)
    else
      ttl
    end
    resource =  %DNS.Resource{class: bufsize,
		   domain: '.',
		   type: :opt,
		   ttl: my_ttl, 
		   data: the_record}
    [resource]
    # Sending two OPT records in a query is forbidden by RFC
    # 6891. It is silent about the case of two OPT records in a
    # response but some clients, like dig, do not like
    # it. [current_resource, future_resource]
  end
  def opt_record(:badversion, _opts, bufsize, _dnssec_ok) do
    [%DNS.Resource{class: bufsize,
		   domain: '.',
		   type: :opt,
		   ttl: 1 * round(:math.pow(2, 24)), # 16 = extended rcode "bad version", see RFC 6891, section 6.1.3
		   data: ''}]
  end
  def opt_record(false, _opts, _bufsize, _dnssec_ok) do
      []
  end

  @spec make_response({:ok, Drink.Parsing.dns_request}, atom, charlist, Drink.Utils.address, integer) :: {:ok, any} | {:error, any}
  @spec make_response({:problem, any}, atom, charlist, Drink.Utils.address, integer) :: {:ok, any} | {:error, any}
  @spec make_response({:error, any}, atom, charlist, Drink.Utils.address, integer) :: {:ok, any} | {:error, any}
  def make_response({:ok, %{:version => :wrongEDNSversion, :header => original_header, :request => original_question}},
		     _proto, _bases, _client, _port) do
    r = %DNS.Record{header:
		    %{original_header |
		      qr: true, aa: false, rcode: Drink.RCodes.noerror},
		    qdlist: original_question,
		    anlist: [],
		    arlist: opt_record(:badversion, %{}, 0)}
    {:ok, DNS.Record.encode(r)}
  end  
  def make_response({:ok, %{:qname => domain, :qtype => type,
			    :header => original_header, :request => original_question,
			    :version => version, :size => bufsize,
			    :dnssec_ok => dnssec_ok,
			    :compact_ok => compact_ok,
			    :edns => edns}},
		       proto, bases, client, port) do
    domain = DomainName.new!(domain)
    Logger.info("#{a2s client} queried for #{DomainName.original(domain)}/#{type}")
    result = DomainName.ends_with(domain, bases)
    {match, base} =
      case result do
	{true, base} -> {true, base}
	false -> {false, nil}
      end
    dnssec_ok = match and dnssec_ok and (config()["dnssec-data"][DomainName.name(base)] != nil)
    response =
    if match do
      {:ok, subdomain} = DomainName.without_suffix(domain, base)
      Drink.Responses.response(DomainName.labels(subdomain), DomainName.original_labels(subdomain), type,
	DomainName.name(base), proto, client, port, edns[:ecs]) 
    else
      %{:rcode => Drink.RCodes.refused, :ede => {20, ""}}
    end
    auth = response[:rcode] == Drink.RCodes.noerror or Drink.RCodes.nxdomain
    # Most of the time, the type in the answer will be the queried
    # type. But not always…
    newtype =  if response[:type] == nil do
      type
    else
      response[:type]
    end
    ttl =  if response[:ttl] == nil do
      @minimum_ttl
    else
      response[:ttl]
    end
    answers =
    if response[:data] == nil do
      []
    else
      if (newtype == :a or newtype == :aaaa) and is_list(response[:data]) do
	Enum.map(response[:data], fn a ->
	  %DNS.Resource{class: :in,
		      domain: to_charlist(DomainName.original(domain)),
                      ttl: ttl,
		      type: newtype,
		      data: a}
	end)
      else
	  [%DNS.Resource{class: :in,
			 domain: to_charlist(DomainName.original(domain)),
			 ttl: ttl,
			 type: newtype,
			 data: response[:data]}]
      end
    end
    if config()["statistics"] do
      Drink.Statistics.post(:rcodes, Drink.RCodes.rcode_to_text(response[:rcode]))
    end
    r = %DNS.Record{header:
		    %{original_header |
		      qr: true, aa: auth,
		      pr: 0, # PR is a mysterious Erlang-specific
			     # thing, vaguely documented in
			     # inet_dns.hrl. We need to set it to
			     # zero, otherwise, we will echo the "Z"
			     # field of the query and this Z field
			     # must always be zero. See bug #71. (Test
			     # with dig +zflag, the answer has a MBZ -
			     # Must Be Zero, which is incorrect.)
		      rcode: response[:rcode]},
		    qdlist: original_question}
    returned_cookies = # See RFC 7873
      case edns[:cookies] do
	nil -> nil # Default. Used if the client sends no cookies.
	{bin, nil} ->
	  {bin, gen_cookie(client, bin)} # Only a client cookie. We are lax, we
			      # don't force clients to send server
			      # cookies, we accept it.
	{bin_c, bin_s} ->
	  if legit_cookie(client, bin_c, bin_s) do
	    # Authenticated client. Today, we don't use this information, we just log it.
	    Logger.info("Client #{a2s(client)} returned a proper cookie") 
	    {bin_c, bin_s}
	  else
	    # Wrong cookie. This may be normal since the client may have an old cookie.
	    Logger.warn("Client #{a2s(client)} sent an invalid/obsolete cookie")
	    {bin_c, bin_s}
	  end
      end
    # zoneversion, unlike most EDNS options, is per-zone (nsid, for
    # instance, is per-server) so the decision to include it or not
    # depend on the response code (actually, it depends on the qname
    # but it is simpler to test the response code).
    add_zoneversion = config()["zoneversion"] and (edns[:zoneversion] != nil and edns[:zoneversion]) and
          (response[:rcode] == Drink.RCodes.noerror or
            response[:rcode] == Drink.RCodes.nxdomain)
    dnssec_config =   
    if dnssec_ok do
      config()["dnssec-data"][DomainName.name(base)]
    else
      nil
    end
    {private_key, key_tag} =
	  if dnssec_ok do
	    {dnssec_config["private-key"],
	     dnssec_config["key-tag"]}
	  else
	    {nil, nil}
	end  
    answers =
    if dnssec_ok and dnssec_config != nil and response[:data] != nil do
      extra = %DNS.Resource{class: :in,
			    domain: to_charlist(DomainName.original(domain)),
			    ttl: ttl,
			    type: Drink.RRTypes.rrsig,
			    data: Drink.Dnssec.sign(domain, base,
			      answers,
			      newtype, ttl,
			      private_key, key_tag)}
      answers ++ [extra]
    else
      answers
    end
    r =
    if response[:add] != nil and response[:add] do
      %{r | arlist: answers ++
	opt_record(version != nil,
	  %{:nsid => edns[:nsid], :zoneversion => add_zoneversion, :cookies => returned_cookies,
	    :reporting => edns[:reporting],
	      :ede => response[:ede]}, config()["server-bufsize"], dnssec_ok)}
    else
	%{r | anlist: answers,
	  arlist: opt_record(version != nil,
	    %{:nsid => edns[:nsid], :zoneversion => add_zoneversion, :cookies => returned_cookies,
	      :reporting => edns[:reporting],
	      :ede => response[:ede]}, config()["server-bufsize"], dnssec_ok)}
    end
    r =
    if (response[:data] == nil and
        (response[:rcode] == Drink.RCodes.noerror or
	  response[:rcode] == Drink.RCodes.nxdomain)) or
       (response[:data] != nil and response[:rcode] == Drink.RCodes.noerror and response[:add]) do
      soa_data = Drink.Responses.my_soa()
      soa = %DNS.Resource{class: :in,
		      domain: to_charlist(DomainName.name(base)),
                      ttl: ttl,
		      type: :soa,
			  data: soa_data}
      if dnssec_ok do
	authority = [soa, %DNS.Resource{class: :in,
				domain: to_charlist(DomainName.name(base)),
				ttl: ttl,
				type: Drink.RRTypes.rrsig,
				data: Drink.Dnssec.sign(base, base,
				  [soa],
				  :soa, ttl,
				  private_key, key_tag)}]
	is_base = DomainName.is_one_of(domain, config()["bases"])
	is_base =
	  case is_base do
	    {true, _base} -> true
	    false -> false
	  end
	authority =
	  cond do
	  response[:rcode] == Drink.RCodes.noerror ->
	    nsec = %DNS.Resource{class: :in,
				 domain: to_charlist(DomainName.original(domain)), 
				 ttl: ttl,
				 type: Drink.RRTypes.nsec,
				 data: Drink.Dnssec.nsec(domain, is_base, false, [type])} 
	    sig = %DNS.Resource{class: :in,
				domain: to_charlist(DomainName.original(domain)),
				ttl: ttl,
				type: Drink.RRTypes.rrsig,
				data: Drink.Dnssec.sign(domain, base,
				  [nsec],
				  Drink.RRTypes.nsec, ttl,
				  private_key, key_tag)}
	    authority ++ [nsec, sig]
	  response[:rcode] == Drink.RCodes.nxdomain ->
	    previous =
	    if not Drink.Dnssec.cde? do
	      Drink.Dnssec.previous_name(domain)
	    else
	      domain
	    end
	    nsec = %DNS.Resource{class: :in,
				 domain: to_charlist(DomainName.name(previous)), 
				 ttl: ttl,
				 type: Drink.RRTypes.nsec,
				 data: Drink.Dnssec.nsec(domain, is_base)} 
	    sig = %DNS.Resource{class: :in,
				domain: to_charlist(DomainName.name(previous)),
				ttl: ttl,
				type: Drink.RRTypes.rrsig,
				data: Drink.Dnssec.sign(previous, base,
				  [nsec],
				  Drink.RRTypes.nsec, ttl,
				  private_key, key_tag)}
	    l = DomainName.labels(domain)
	    qname_truncated = Enum.slice(l, 1, length(l))
	    wildcard = DomainName.join!(["*" | qname_truncated])
	    previous = Drink.Dnssec.previous_name(wildcard)
	    if not Drink.Dnssec.cde? do
	      nsec_wildcard = %DNS.Resource{class: :in,
					  domain: to_charlist(DomainName.name(previous)), 
					  ttl: ttl,
					  type: Drink.RRTypes.nsec,
					  data: Drink.Dnssec.nsec(wildcard)} 
	    sig_wildcard = %DNS.Resource{class: :in,
					 domain: to_charlist(DomainName.name(previous)),
					 ttl: ttl,
					 type: Drink.RRTypes.rrsig,
					 data: Drink.Dnssec.sign(previous, base,
					   [nsec_wildcard],
					   Drink.RRTypes.nsec, ttl,
					   private_key, key_tag)}
	      authority ++ [nsec, sig, nsec_wildcard, sig_wildcard]
	    else
	      authority ++ [nsec, sig]
	    end
	  true ->
	    authority
	  end
	%{r | nslist: authority}
      else
	  %{r | nslist: [soa]}
      end
    else
      addr_v4 = Drink.Responses.includes_v4(config()["nserver-addresses"])
      addr_v6 = Drink.Responses.includes_v6(config()["nserver-addresses"])
      if response[:data] != nil and response[:rcode] == Drink.RCodes.noerror and type == :ns and
             (addr_v4 != [] or addr_v6 != []) do
	addr =
	if addr_v4 != [] do
	  Enum.map(addr_v4, fn a -> %DNS.Resource{class: :in,
		      domain: to_charlist(DomainName.name(config()["nserver"])),
                      ttl: ttl,
		      type: :a,
		      data: a} end)
	else
	  []
	end
	addr =
	if dnssec_ok and dnssec_config != nil and addr != [] do
	  signature = %DNS.Resource{class: :in,
			    domain: to_charlist(DomainName.name(config()["nserver"])),
			    ttl: ttl,
			    type: Drink.RRTypes.rrsig,
			    data: Drink.Dnssec.sign(config()["nserver"], base,
			      addr,
			      :a, ttl,
			      private_key, key_tag)}
	  addr ++ [signature]
	else
	  addr
	end
	addr_only_v6 =
	if addr_v6 != [] do
	  Enum.map(addr_v6, fn a -> %DNS.Resource{class: :in,
		      domain: to_charlist(DomainName.name(config()["nserver"])),
                      ttl: ttl,
		      type: :aaaa,
		      data: a} end)
	else
	  []
	end
	addr =
	if dnssec_ok and dnssec_config != nil and addr_only_v6 != [] do
	  signature = %DNS.Resource{class: :in,
			    domain: to_charlist(DomainName.name(config()["nserver"])),
			    ttl: ttl,
			    type: Drink.RRTypes.rrsig,
			    data: Drink.Dnssec.sign(config()["nserver"], base,
			      addr_only_v6,
			      :aaaa, ttl,
			      private_key, key_tag)}
	  addr ++ addr_only_v6 ++ [signature]
	else
	  addr ++ addr_only_v6
	end
	%{r | arlist: r.arlist ++ addr}
      else
	r
      end
    end
    r =
       if dnssec_ok and dnssec_config != nil and Drink.Dnssec.cde? and
          r.header.rcode == Drink.RCodes.nxdomain and not compact_ok do
	  %{r | header: %{r.header | rcode: Drink.RCodes.noerror}}
       else
	    r
       end
    r_encoded = DNS.Record.encode(r)
    r =
    if config()["padding"] and (edns[:padding] != nil and edns[:padding]) and proto == :tls do
      # Padding is tricky because we need to do it at the end, when we
      # know the size of the response. And it requires patching an
      # already existing EDNS record.
      response_size = byte_size(r_encoded)
      num_pads = floor(response_size/@pad_block_size) + 1
      bytes = num_pads*@pad_block_size - 4 - response_size # 4 is for the
      # Type and Length of the EDNS option.
      bits = bytes*8
      padding =
	Binary.append(<<Drink.EdnsCodes.padding::unsigned-integer-size(16),
	  bytes::unsigned-integer-size(16)>>, <<0::size(bits)>>)
      opt = Enum.at(Enum.filter(r.arlist, fn r -> r.type == :opt end), 0)
      others = Enum.filter(r.arlist, fn r -> r.type != :opt end)
      opt = %{opt | data: opt.data <> padding}
      %{r | arlist: others ++ [opt]}
    else
      r
    end
    if config()["statistics"] do
      question = Enum.at(r.qdlist, 0)
      Drink.Statistics.post(:qtypes, String.upcase(Drink.RRTypes.rrtype_to_text(question.type)))
      qname = String.downcase(to_string(question.domain))
      name=
	case r.header.rcode do
	  # We won't log all the domain names, otherwise, an attacker
	  # could easily DoS us with random QNAMEs.
	  0 -> qname
	  _ -> "OTHER NAME"
	end
      Drink.Statistics.post(:qnames, name)
    end
    r_encoded = DNS.Record.encode(r)
    if proto != :udp or
       (byte_size(r_encoded) <= bufsize and byte_size(r_encoded) <= config()["server-bufsize"]) do
      {:ok, r_encoded}
    else
      r = %DNS.Record{header:
		      %{original_header |
			tc: true, qr: true, aa: auth, rcode: response[:rcode]},
		      qdlist: original_question,
		      anlist: [],
		      arlist: opt_record(version != nil,
			%{:nsid => edns[:nsid], :zoneversion => edns[:zoneversion], :cookies => returned_cookies})}
      {:ok, DNS.Record.encode(r)}
    end
  end
  def make_response({:problem, reason, %{:header => original_header, :request => original_question}},
		     _proto, _base, _client, _port) do
    r = %DNS.Record{header:
		    %{original_header |
		      qr: true, aa: false, rcode: Drink.RCodes.formerr},
		    qdlist: original_question,
		    anlist: [],
		    arlist: []}
    {:problem, reason, DNS.Record.encode(r)}
  end  
  def make_response({:error, reason}, _proto, _base, _client, _port) do
    {:error, reason}
  end

  @spec write({:ok, any}, :udp|:tcp|:tls, integer, Drink.Utils.address, integer | {:error, any}) ::
             :ok | {:error, any}
  def write({:ok, data}, :udp, socket, client, port) do
    Logger.debug("Sending answer to #{a2s client}")
    :gen_udp.send(socket, client, port, data)
  end  
  def write({:ok, data}, :tls, socket, client, _port) do
    length = byte_size(data)
    Logger.debug("Sending answer of #{length} bytes to #{a2s client}")
    :ssl.send(socket, data)
  end
  def write({:ok, data}, :tcp, socket, client, _port) do
    length = byte_size(data)
    Logger.debug("Sending answer of #{length} bytes to #{a2s client}")
    :gen_tcp.send(socket, data)
  end
  def write({:problem, reason, data}, :udp, socket, client, port) do
    Logger.info("Replying FORMERR to #{a2s client} because #{reason}")
    :gen_udp.send(socket, client, port, data)
  end
  def write({:problem, reason, data}, :tcp, socket, client, _port) do
    Logger.info("Replying FORMERR to #{a2s client} because #{reason}")
    :gen_tcp.send(socket, data)
  end
  def write({:error, reason}, _protocol, _socket, client, _port) do
    Logger.info("We ignore #{a2s client} because #{reason}")
  end

  @spec gen_cookie(Drink.Utils.address, binary) :: binary
  def gen_cookie(client_address, client_cookie) do
    # RFC 7873, section 4.2.
    one_byte = tuple_size(client_address) == 4
    addr = Enum.reduce(Tuple.to_list(client_address), [],
      fn d, acc ->
	if one_byte do
	  acc ++ [d]
	else
	  if d >= 256 do
	    high = trunc(d/256)
	    acc ++ [high, d-(high*256)]
	  else
	    acc ++ [0, d]
	  end
	end
      end)
    Binary.part(:crypto.hash(:sha256, config()["secret-salt-for-cookies"] <> Binary.from_list(addr) <> client_cookie), 0, 8)
  end

  @spec legit_cookie(Drink.Utils.address, binary, nil | binary) :: boolean
  def legit_cookie(client_address, client_cookie, server_cookie) do
    server_cookie == gen_cookie(client_address, client_cookie)
  end
  
end


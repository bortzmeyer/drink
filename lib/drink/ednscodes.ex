defmodule Drink.EdnsCodes do
  @moduledoc """
  The DNS library we use has unfortunately no [EDNS
  support](https://github.com/tungd/elixir-dns/issues/48). We list
  here the codes for EDNS options. The authoritative list is [at
  IANA](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-11).
  """

  defmacro nsid do
    3
  end
  defmacro ecs do
    8
  end
  defmacro expire do
    9
  end
  defmacro cookie do
    10
  end
  defmacro padding do
    12
  end
  defmacro extended do
    15
  end
  defmacro reporting do
    18
  end
  defmacro zoneversion do
    19 
  end
  
end


  

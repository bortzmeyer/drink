defmodule Drink.Responses.Report do
  @moduledoc """
  RFC 9567
  DNS Error Reporting
  """

  import Drink.Config
  require Logger
  require Drink.RCodes
  require Drink.Responses

  @report_ttl 30 # Experimental, so low TTL

  def response(qname, qtype) do
    if config()["services"]["report"] do
      case qtype do
	:txt ->
	  result = Drink.Reports.post(Enum.join(qname, "."))
	  %{:rcode => Drink.RCodes.noerror,
	    :data => [result],
	    :ttl => @report_ttl}
	:any -> Drink.Responses.any_hinfo()
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
  
  def dump(qtype) do
    if config()["services"]["report"] != nil and config()["report-via-dns"] do
      case qtype do
	:txt ->
	  result = Drink.Reports.get()
	  if result != [] do
	    %{:rcode => Drink.RCodes.noerror,
	      :data => result,
	      :ttl => @report_ttl}
	  else
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => @report_ttl}
	  end
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

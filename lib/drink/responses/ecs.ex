defmodule Drink.Responses.Ecs do
  import Drink.Config
  import Drink.Utils
  require Drink.RCodes
  require Drink.Responses
  def response(qtype, ecs) do
    if config()["services"]["ecs"] do
      case qtype do
	:txt ->
	  data =
	  if ecs != nil do
	    "#{a2s(elem(ecs, 0))}/#{elem(ecs, 1)}"
	  else
	    ""
	  end
	  %{:rcode => Drink.RCodes.noerror,
	    :data => [data]}
	:any -> Drink.Responses.any_hinfo()
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

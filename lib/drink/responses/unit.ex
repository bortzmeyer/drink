defmodule Drink.Responses.Unit do
  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  @unit_ttl 86400 # Conversion does not change
  def response(qname, orig_qname, qtype) do
    if config()["services"]["unit"] do
      case qtype do
	:txt ->
	  cond do
	    length(qname) > 3 ->
	      %{:rcode => Drink.RCodes.nxdomain}
	    length(qname) == 1 ->
	      %{:rcode => Drink.RCodes.noerror, :ttl => @unit_ttl,
		:data => ["See https://github.com/carturoch/ex_uc for the possible unit conversions"]}
	    length(qname) == 2 ->
	      %{:rcode => Drink.RCodes.noerror}
	    True ->
	      from = Enum.at(orig_qname, 0)
	      to = Enum.at(orig_qname, 1)
	      response =
		try do
		  ExUc.convert(from, to)
		rescue
		  _e in MatchError -> # https://github.com/carturoch/ex_uc/issues/7
                       "unknown origin #{inspect from}"
		end
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => @unit_ttl,
		:data => [response]}
	  end
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

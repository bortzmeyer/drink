defmodule Drink.Responses.Connection do
  import Drink.Utils
  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  def format_protocol(p) do
    s = "#{p}"
    "over #{String.upcase(s)}"
  end
  def response(qtype, protocol, client, port) do
    if config()["services"]["connection"] do
      protocol = format_protocol(protocol)
      case {qtype, address_family(client)} do
	{:txt, :inet4} -> %{:rcode => Drink.RCodes.noerror, :data => ["#{a2s(client)}:#{port} #{protocol}"]}
	{:txt, :inet6} -> %{:rcode => Drink.RCodes.noerror, :data => ["[#{a2s(client)}]:#{port} #{protocol}"]}
	{:any, _} -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror} # NODATA
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

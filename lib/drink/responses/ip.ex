defmodule Drink.Responses.Ip do
  import Drink.Utils
  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  def response(qtype, client) do
    if config()["services"]["ip"] do
      case {qtype, address_family(client)} do
	{:a, :inet4} ->
	  %{:rcode => Drink.RCodes.noerror, :data => unmap(client)} 
	  {:a, :inet6} ->
	  %{:rcode => Drink.RCodes.noerror, :data => client, :type => :aaaa,
	    :add => true} 
	  {:aaaa, :inet4} ->
	  %{:rcode => Drink.RCodes.noerror, :data => unmap(client),
	    :type => :a, :add => true} 
	  {:aaaa, :inet6} ->
	  %{:rcode => Drink.RCodes.noerror, :data => client} 
	  {:txt, _} -> %{:rcode => Drink.RCodes.noerror, :data => [a2s(client)]}
	{:any, _} -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror} # NODATA
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

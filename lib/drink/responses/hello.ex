defmodule Drink.Responses.Hello do
  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  def response(qtype) do
    if config()["services"]["hello"] do
      case qtype do
	# A TXT record value is *several* strings. Hence the list.
	:txt -> %{:rcode => Drink.RCodes.noerror,
		 :data => ['Hello, this is the Drink DNS server, version ' ++ Application.spec(:drink, :vsn) ++
			    ' and I use the dns library version ' ++ Application.spec(:dns, :vsn) ++
			    ', running on Elixir ' ++ System.version()
			  ]}
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

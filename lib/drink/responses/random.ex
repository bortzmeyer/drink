defmodule Drink.Responses.Random do

  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  require Drink.Types
  
  # Returns between -90 and 90
  @spec random_latitude() :: float
  def random_latitude() do
    Enum.random(0..324000)/1800-90
  end
  
  # Returns between -180 and 180
  @spec random_longitude() :: float
  def random_longitude() do
    Enum.random(0..648000)/1800-180
  end
  
  # Returns between 0 and some arbitrary value
  @spec random_altitude() :: float
  def random_altitude() do
    Enum.random(0..10000)
  end
  
  def response(qtype) do
    if config()["services"]["random"] do
      case qtype do
	:txt -> %{:rcode => Drink.RCodes.noerror,
		 :data => [to_charlist(to_string(Enum.random(1..config()["max-rand-value"])))]}
	:a -> %{:rcode => Drink.RCodes.noerror,
	       :data => List.to_tuple(Enum.map(1..4, fn _x -> Enum.random(0..255) end))}
	:aaaa -> %{:rcode => Drink.RCodes.noerror,
		  :data => List.to_tuple(Enum.map(1..8, fn _x -> Enum.random(0..65535) end))}
	Drink.Types.loc -> %{:rcode => Drink.RCodes.noerror, :type => Drink.Types.loc,
		 :data => Drink.Utils.loc(random_latitude(), random_longitude(),
		  random_altitude())}
	:uri ->
	  %{:rcode => Drink.RCodes.noerror,
	    :data => {10, 1,
		      "http://www.openstreetmap.org/?mlat=#{random_latitude()}&mlon=#{random_longitude()}&zoom=6"}}
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

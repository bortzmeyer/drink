defmodule Drink.Responses.Date do
  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  def response(qtype) do
    if config()["services"]["date"] do
      case qtype do
	:txt -> %{:rcode => Drink.RCodes.noerror,
		 :data => [to_charlist(DateTime.to_iso8601(DateTime.utc_now()))]}
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end

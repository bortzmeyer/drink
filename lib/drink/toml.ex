defmodule Drink.Toml do

  import Drink.Defaults

  @spec load_config(binary | [binary], boolean) :: {:error, :enoent} | {:ok, %{}}
  def load_config(filename, must_exist \\ false) do
    result = TomlElixir.parse_file(filename) # https://hexdocs.pm/toml_elixir
    case result do
      {:ok, config} -> {:ok, config}
      {:error, :enoent} -> # Dialyzer claims "The pattern can never
			   # match the type" which is obviously false,
			   # we have it when the file does not
			   # exist. This is apparently a bug in the
			   # Toml package
			   # https://github.com/bitwalker/toml-elixir/issues/33
	if must_exist do
	  exit("Non existing configuration file #{filename}")
	else
	  {:error, :enoent}
	end
      other -> exit("Unknown error #{inspect other} in configuration file #{filename}")
    end
  end

  @spec load_configs(binary | [binary]) :: {:error, :enoent} | {:ok, %{}}
  def load_configs(list \\ config_files())
  def load_configs([filename | rest]) do
    result = load_config(filename, false)
    case result do
      {:ok, config} -> {:ok, config}
      {:error, :enoent} -> 
	load_configs(rest)
    end
  end
  def load_configs([]) do
    {:ok, %{}} # Configuration file is not mandatory
  end
  def load_configs(s) do
    list = String.split(s, ":", trim: true)
    load_configs(list)
  end
  
end


defmodule Drink.Dnssec do

  require Drink.RCodes
  require Drink.RRTypes
  
  @algorithm 8 # Currently hardwired to RSASHA256, see issue #49.
  @duration 360000
  @margin 30000

  @cde false # Compact Denial of Existence
	     # draft-ietf-dnsop-compact-denial-of-existence-03
	     # (formerly "black lies"). # Careful if you enable it,
	     # the tests no longer work today (2024-03-17) with
	     # CDE. See dnssec-tests/cde-tests.
  def cde? do 
    @cde
  end
  
  @rfc4470epsilon false # The epsilon function recommended by the RFC
		        # makes Google Public DNS and BIND servfail
		        # for denial of existence (and other resolvers
		        # to turn NXDOMAIN into NOERROR). So, even if
		        # 'drill -S' accepts it (and therefore tests
		        # pass), it should not be used.

  def rfc4470epsilon? do
    @rfc4470epsilon
  end
  
  @doc """
  Returns the DNSSEC signature of 'data' (which must be a list of resource records). 
  """
  @spec sign(DomainName.t(), DomainName.t(), [%DNS.Resource{}], atom | integer, integer, binary, integer) :: binary
  def sign(name, owner, data, type, ttl, key, tag) do
    l = DomainName.labels(name)
    num_labels =
    if List.first(l) == "*" do
      length(l)-1
    else
      length(l)
    end
    ntype = Drink.RRTypes.rrtype_to_num(type)
    now = div(System.os_time(:second), 86400)*86400
    inception = now - @margin
    expiration = now + @duration
    # RFC 4034, section 3.1.8.1
    owner_bin = Binary.from_list(DomainName.encode(owner))
    short_rrsig =  <<ntype::unsigned-integer-size(16), @algorithm::unsigned-integer-size(8), num_labels::unsigned-integer-size(8),
      ttl::unsigned-integer-size(32), expiration::unsigned-integer-size(32), inception::unsigned-integer-size(32),
      tag::unsigned-integer-size(16)>>
      |> Binary.append(owner_bin)
    encoded_rrset = Drink.Encoding.encode(data)
    {:ok, sig} = ExPublicKey.sign(Binary.append(short_rrsig, encoded_rrset), key) 
    short_rrsig |> Binary.append(sig)
  end

  # White lies (RFC 4470) or black lies (compact denial of existence)
  @spec next_name(DomainName.t()) :: DomainName.t()
  def next_name(s) do
    labels = DomainName.labels(s)
    first = List.first(labels)
    rest = DomainName.join!(Enum.slice(labels, 1, length(labels)))
    if @rfc4470epsilon or @cde do
       DomainName.join!("\x00", s)
    else
      if first == "*" do # Wildcard
	if length(labels) <= 1 do
	  raise ArgumentError, "next name of just a wildcard???"
	end
        DomainName.join!("-", rest)
      else
	if length(labels) == 1 do 
	  DomainName.join!("0", s) # Domain was a TLD
	else
          DomainName.join!("#{first}!", rest)
	end
      end
    end
  end
  @spec previous_name(DomainName.t()) :: DomainName.t()
  def previous_name(s) do
    labels = DomainName.labels(s)
    first = List.first(labels)
    rest = DomainName.join!(Enum.slice(labels, 1, length(labels)))
    <<c::utf8>> = String.last(first)
    c = c - 1
    e = <<c::utf8>>
    begin =
    if String.length(first) > 1 do
       String.slice(first, 0..String.length(first)-2)
    else
      ""
    end
    new_s = begin <> e
    new_f =
    if @rfc4470epsilon do
      pad_size = 63 - String.length(first)
      String.pad_trailing(new_s, pad_size, "~") # ~ is the last ASCII
			# character. It would be better to use ÿ
			# (U+00FF) but the DomainName package
			# complains because it does not handle IDN
			# <https://framagit.org/bortzmeyer/domainname-elixir/-/issues/2>
			# and have no "escape" to work with 8-bits
			# characters. Also, some conversions may turn
			# the UTF-8 in two code points, breaking the
			# signature (to_charlist returns the proper
			# result). In the mean time, use only ASCII.
    else 
      if first == "*" do # Wildcard
         "!~" 
      else
         "~." <> new_s <> "~" 
      end
    end
    DomainName.join!(new_f, rest)
  end
  
  @doc """
  Returns an encoded NSEC record for 'name'. 'without' is a list of RR
  types to exclude (typically one one item, the one which was
  requested and which triggered a NODATA answer).
  """
  @spec nsec(DomainName.t(), boolean, boolean, [atom]) :: binary
  def nsec(name, is_base \\ false, nxdomain \\ true, without \\ []) do
    minimum_types_list = [Drink.RRTypes.rrsig, Drink.RRTypes.nsec]

    types_list =
    if nxdomain do
      if not cde?() do
	minimum_types_list
      else
	[Drink.RRTypes.nxname | minimum_types_list]
      end
    else
      default_list = [:a, :aaaa, :mx, :txt, :ns, :soa, :uri, 
		      Drink.RRTypes.dnskey, Drink.RRTypes.loc, Drink.RRTypes.rrsig, Drink.RRTypes.nsec, Drink.RRTypes.wallet]
      # We indicate all the RR types we may handle.
      Enum.filter(default_list,
	fn t ->
	  (t not in without) and
	  (is_base or (t != :ns and t != :soa and t != Drink.RRTypes.dnskey)) # PowerDNS recursor requires that, see #47
	end) 
      |> Enum.map(fn t -> Drink.RRTypes.rrtype_to_num(t) end)
    end
    next = next_name(name)
    Binary.append(Binary.from_list(DomainName.encode(next)),
      nsec_bitmap(types_list))
    end

  def bits_of([], _n) do
    []
  end
  
  def bits_of([first | rest], n) do
    List.duplicate(0, first-n) ++ [1] ++ bits_of(rest, first+1)
  end
  
  @doc """
  Input is a list of numbers representing the RR types. Output is the
  bitmap. RFC 4034, section 4.1.
  """
  @spec nsec_bitmap([integer]) :: binary
  def nsec_bitmap([]) do
    <<>>
  end
  def nsec_bitmap(l) do
    l =
    Enum.map(l, fn t -> Drink.RRTypes.rrtype_to_num(t) end)
    |> Enum.sort
    block = floor(Enum.min(l)/256)
    bits = 
    Enum.filter(l, fn type -> type < (256*(block+1)) end)
    |> Enum.map(fn type -> type - (256*block) end)
    |> bits_of(0)
    remainder = rem(length(bits), 8)
    pad_size =
    if remainder == 0 do
      0
    else
      8 - remainder
    end
    bits = bits ++ List.duplicate(0, pad_size)
    values = Enum.into(bits, <<>>, fn bit -> <<bit::1>> end)
    rest = Enum.filter(l, fn type -> type >= (256*(block+1)) end)
    Binary.append(Binary.append(<<block::unsigned-integer-size(8), byte_size(values)::unsigned-integer-size(8)>>,
	  values),
      nsec_bitmap(rest))
  end
  
end

defmodule Drink.Defaults do
  # If you modify default values, do not forget to update example.toml.
  @default_config %{"port" => 53, "base" => "test", "nameserver" => "ns1.test=::1", "maintainer" => "root.invalid",
		    "logging-facility" => "local0", "logging-level" => "warning", "console" => true,
		    "bind" => [:any], "negative-ttl" => 86400,
		    "server-bufsize" => 1440, "default-nonnull-ttl" => 30,
		    "secret-salt-for-cookies" => "I hate wasabi",
		    "dnssec_key" => nil, "dnssec_key_tag" => nil,
		    "ipc-socket" => nil,
		    "wallet" => nil,
		    "services" => ["date", "random", "ip", "connection", "hello", "ecs", "unit", "op"],
		    "ipv4-only" => false, "ipv6-only" => false, "max-random-value" => 1000,
		    "statistics" => false, # False by default since
					   # they can probably
					   # seriously slow down the
					   # server
		    "dot-cert" => nil, "dot-key" => nil, "dot-port" => 853,
		    "padding" => true,
		    "reporting-agent" => nil, # RFC 9567, DNS Error Reporting
		    "report-via-dns" => false,
		    "zoneversion" => false, # RFC 9660, The DNS Zone Version (ZONEVERSION) Option
                    # Now the IETF experimental stuff
		    "greasing" => false
}
  # This is to be able to warn early if an unknown service is requested. See issue #63.
  def possible_services do
    ["date", "random", "ip", "connection", "hello", "ecs", "country", "unit", "op", "weather", "number", "bgp", "report"]
  end
  @config_file_home System.get_env("HOME") <> "/.drink.toml"
  @config_file_etc "/etc/drink.toml"
  def config_files do
     [@config_file_home, @config_file_etc]
  end
  def default_config do
    @default_config
  end
  def geoip_provider do
    :ipinfo # https://ipinfo.io/
  end
end

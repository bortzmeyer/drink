defmodule Drink.Responses do

  @moduledoc """
  Here is the call to business logic. We define the dynamic answers
  for the base domain and we call modules under `Drink.Responses` for
  the services linked to subdomains.  
  """
  
  import Drink.Config
  
  require Drink.RCodes
  require Drink.RRTypes
  
  require Logger

  defmacro any_hinfo do
    Macro.escape(%{:rcode  => Drink.RCodes.noerror, :type => :hinfo, :data => {'RFC8482', ''}})
  end

  # DNSSSEC constants
  @dnssec_flags 257 # We need a KSK
  @dnssec_version 3
  @dnssec_algorithm 8 # Hardwired to RSA-SHA256, see issue #49.
  
  # Response to a DNS query 
  @type dns_response  :: %{
    required(:rcode) => integer, # DNS return code, see Drink.RCodes
    optional(:data) => any, # Default is NODATA
    optional(:type) => integer, # DNS types, such as :aaaa or :ns. Default is the one in the query
    optional(:ttl) => integer, # Default is set in server.ex/@minimum_ttl except for some responses
    optional(:add) => boolean, # true if data is meant for the
			       # Additional section. Default is false
                               # (meant for the Answer section)
    optional(:ede) => {integer, String.t()} # Extended DNS Error: a code and optional text string
  }

  def my_soa() do
    {to_charlist(DomainName.name(config()["nserver"])), 
     to_charlist(DomainName.name(config()["maintainer"])), 
     Drink.Utils.serial(),          # Serial number
     # We operate with a single authoritative
     # server so the next three parameters
     # are useless and currently not tunable.
     1800,            # Refresh every 30 min 
     300,             # Retry every 5 min
     604800,          # Expire after a week
     config()["negative-ttl"]            # Minimum
    }
  end

  @spec includes_v4([Drink.Utils.address]) :: [Drink.Utils.address]
  def includes_v4(t) do
    Enum.filter(t, fn a -> Drink.Utils.address_family(a) == :inet4 end)
  end

  @spec includes_v6([Drink.Utils.address]) :: [Drink.Utils.address]
  def includes_v6(t) do
    Enum.filter(t, fn a -> Drink.Utils.address_family(a) == :inet6 end)
  end
  
  @doc """ 
  qname : Query NAME, as a list of strings (excluding the base
  domain), you need to provide both a downcased qname (for ease of
  comparison) and the original one because some services (such as
  unit), need to know the original case (see
  <https://github.com/knadh/dns.toys/issues/41>), qtype : Query TYPE
  (AAAA, MX, etc) Returns a dns_response (see its description above). 

  Special note for types A and AAAA: if you return as data a list, the
  server will return several DNS records (a Resource Record set).
  """
  @spec response([String.t()], [String.t()], integer, String.t(), atom, Drink.Utils.address, integer, {}) :: dns_response
  def response([], [], qtype, base, _protocol, _client, _port, _ecs) do # Apex of the base domain
    case qtype do
      :txt ->
	doc = ['Possible queries: '] ++
	if config()["services"]["hello"] do
	  ['hello/TXT to have a greeting. ']
	else
	  ''
	end ++
	if config()["services"]["ip"] do
	  ['ip/TXT,A,AAAA to have the IP address of the client. ']
	else
	  ''
	end ++
	if config()["services"]["connection"] do
	  ['connection/TXT to have the IP address, transport protocol and port of the client. ']
	else
	  ''
	end ++
	if config()["services"]["date"] do
	  ['date/TXT to have the date. ']
	else
	  ''
	end ++
	if config()["services"]["random"] do
	  ['random/TXT,A,AAAA to have a random integer or address. ']
	else
	  ''
	end ++
	if config()["services"]["bgp"] do
	  ['bgp/TXT to have the IP address of the client, its prefix in the DFZ and its origin AS number. ']
	else
	  ''
	end ++
	if config()["services"]["country"] do
	  ['country/TXT to have the country of the client. ']
	else
	  ''
	end ++
	if config()["services"]["number"] do
	  ['NUMBER.LANG.number/TXT to have the number spelled in the mentioned language. ']
	else
	  ''
	end ++
	if config()["services"]["weather"] do
	  ['CITY.now.weather/TXT to have today\'s weather and CITY.tomorrow.weather for the forecast. ']
	else
	  ''
	end ++
	if config()["services"]["unit"] do
	  ['Xorigin.Ydestination.unit/TXT to do unit conversions. ']
	else
	  ''
	end ++
	if config()["services"]["op"] do
	  ['Expr.op/TXT to evaluate an arithmetic expression. ']
	else
	  ''
	end ++
	if config()["services"]["country"] != nil and config()["services"]["bgp"] != nil and config()["services"]["ip"] != nil do
	  ['full/TXT to have IP address, AS and country of the client. ']
	else
	  ''
	end 
	%{:rcode => Drink.RCodes.noerror, :data => doc}
      :ns ->  %{:rcode => Drink.RCodes.noerror,
	       :data => to_charlist(DomainName.name(config()["nserver"])),
	       :ttl => config()["default-nonnull-ttl"]}
      :soa ->  %{:rcode => Drink.RCodes.noerror,
		:data => my_soa()
		}
      :mx ->
	if config()["mailserver"] != nil and config()["mailserver"] != "" do
	  %{:rcode => Drink.RCodes.noerror,
	    :ttl => config()["default-nonnull-ttl"],
	    :data =>  {10, to_charlist(DomainName.name(config()["mailserver"]))}}
	else
	    %{:rcode => Drink.RCodes.noerror,
	      :ttl => config()["default-nonnull-ttl"],
	      :data =>  {0, to_charlist(".")}} # RFC 7505
	end
      Drink.RRTypes.dnskey ->
	if config()["dnssec"] != nil and config()["dnssec"] != "" do
	  case config()["dnssec-data"][base] do
	    nil ->  %{:rcode => Drink.RCodes.noerror,
		     :ttl => config()["default-nonnull-ttl"]}
	    this_zone ->
              pub_key = this_zone["public-key"]		   
	      # The documentation of ExPublicKey seems wrong about the
	      # order of exponent and modulus…
	      {:ok, {:RSAPublicKey, modulus, exponent}} = ExPublicKey.RSAPublicKey.as_sequence(pub_key)
	      # RSA 3110, section 2
	      exponent = Binary.from_integer(exponent) 
	      modulus = Binary.from_integer(modulus)
	      length = byte_size(exponent)
	      length_size =
	      if length > 255 do
		3*8
	      else
		8
	      end
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => config()["default-nonnull-ttl"],
		# RFC 4034, section 2.1
		:data => Binary.append(Binary.append(<<@dnssec_flags::unsigned-integer-size(16),
		      @dnssec_version::unsigned-integer-size(8),
		      @dnssec_algorithm::unsigned-integer-size(8),
		      length::unsigned-integer-size(length_size)>>,
		      exponent), modulus)}
	  end
	else
	    %{:rcode => Drink.RCodes.noerror,
	      :ttl => config()["default-nonnull-ttl"]}
	end
      Drink.RRTypes.wallet ->
         if config()["wallet"] != nil and config()["wallet"] != [] do
               %{:rcode => Drink.RCodes.noerror,
	        :data => Drink.encoding.encode(config()["wallet"]),
	        :ttl => config()["default-nonnull-ttl"]}
         else
	    %{:rcode => Drink.RCodes.noerror,
	      :ttl => config()["default-nonnull-ttl"]}
	 end
      Drink.RRTypes.nxname -> 
	%{:rcode => Drink.RCodes.formerr, :ede => {18, ""}}
      :any -> any_hinfo()
      :axfr -> %{:rcode => Drink.RCodes.refused}
      _ -> %{:rcode => Drink.RCodes.noerror}
    end
  end
  def response(qname, orig_qname, qtype, _base, protocol, client, port, ecs) do
    # Most services do not have parameters in the qname, they just use
    # the service name. Therefore, any name with several labels elicit
    # a NXDOMAIN. Some services have parameters and are listed here as
    # exceptions:
    if Drink.Dnssec.cde? and qtype == Drink.RRTypes.nxname do
      %{:rcode => Drink.RCodes.formerr, :ede => {18, ""}}
    else
	if config()["nserver-in-zone"] and qname == config()["nserver-short"] do
	    case qtype do
	      :a ->
		if includes_v4(config()["nserver-addresses"]) != [] do
		  %{:rcode => Drink.RCodes.noerror, :data => includes_v4(config()["nserver-addresses"]),
		      :ttl => config()["default-nonnull-ttl"]}
		else
		  %{:rcode => Drink.RCodes.noerror}
		end
	      :aaaa -> 
		if includes_v6(config()["nserver-addresses"]) != nil do
		  %{:rcode => Drink.RCodes.noerror, :data => includes_v6(config()["nserver-addresses"]),
		      :ttl => config()["default-nonnull-ttl"]}
		else
		  %{:rcode => Drink.RCodes.noerror}
		end
		_ -> %{:rcode => Drink.RCodes.noerror}
	    end
	else
	  if (List.last(qname) not in ["unit"] and List.last(qname) not in ["op"] and
	    List.last(qname) not in ["weather"] and	List.last(qname) not in ["number"] and
	    List.last(qname) not in ["report"]) and length(qname) > 2 do
	    %{:rcode => Drink.RCodes.nxdomain} 
	  else
	    # If you add a service here, do not forget to add it also in
	    # the function possible_services of defaults.ex. See issue #63.
	    case List.last(qname) do  
	      "hello" ->
		Drink.Responses.Hello.response(qtype)
	      "ip" ->
		Drink.Responses.Ip.response(qtype, client)
	      "connection" ->
		Drink.Responses.Connection.response(qtype, protocol, client, port)
	      "date" ->
		Drink.Responses.Date.response(qtype)
	      "ecs" ->
		Drink.Responses.Ecs.response(qtype, ecs)
	      "random" ->
		Drink.Responses.Random.response(qtype)
	      "bgp" ->
		Drink.Responses.Bgp.response(qtype, client)
	      "country" ->
		Drink.Responses.Country.response(qtype, client)
	      "full" ->
		Drink.Responses.Full.response(qtype, client)
	      "weather" ->
		Drink.Responses.Weather.response(qname, qtype)
	      "number" ->
		Drink.Responses.Number.response(qname, qtype)
	      "report" ->
		Drink.Responses.Report.response(qname, qtype)
	      "dump-report" ->
		Drink.Responses.Report.dump(qtype)
	      "unit" ->
		Drink.Responses.Unit.response(qname, orig_qname, qtype)
	      "op" ->
		Drink.Responses.Op.response(qname, qtype)
	      _ -> %{:rcode => Drink.RCodes.nxdomain}
	    end
	end
      end
    end
  end
end



defmodule Drink.RRTypes do
  @moduledoc """

  The DNS library we use has unfortunately no way to translate atoms such as :a or :txt to numeric values, but we need these values for DNSSEC. So, we do it ourselves. The authoritative list is [at
  IANA](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-6).

  """

  # Values not currently known by the DNS library we use
  defmacro loc do
    29
  end
  defmacro rrsig do
    46
  end
  defmacro nsec do
    47
  end
  defmacro dnskey do
    48
  end
  defmacro resinfo do
    261
  end
  defmacro wallet do
    262
  end
  
  # Experimental IETF stuff
  @nxname 65283 # draft-ietf-dnsop-compact-denial-of-existence-03
  defmacro nxname do
    @nxname
  end
  
  def rrtype_to_num(type) do
    case type do
      :a -> 1
      :ns -> 2
      :cname -> 5
      :soa -> 6
      :null -> 10
      :hinfo -> 13
      :mx -> 15
      :txt -> 16
      :aaaa -> 28
      :naptr -> 35
      :srv -> 33
      :uri -> 256
      :resinfo -> 261
      :wallet -> 262
      :nxname -> @nxname
      _ -> if is_integer(type), do: type, else: nil
    end
  end
  
  def rrtype_to_text(type) when is_atom(type) do
      "#{type}"
  end

  def rrtype_to_text(type) when is_integer(type) do
    case type do
      29 -> "loc"
      43 -> "ds"
      46 -> "rrsig"
      47 -> "nsec"
      48 -> "dnskey"
      51 -> "nsec3param"
      52 -> "tlsa"
      59 -> "cds"
      60 -> "cdnskey"
      64 -> "svcb"
      65 -> "https"
      @nxname -> "nxname"
      261 -> "resinfo"
      262 -> "wallet"
      _ -> "#{type}"
    end
  end
  
end



  

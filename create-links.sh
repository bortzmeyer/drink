#!/bin/sh

set -e

version=$(elixir --version | head -n 1 | cut -d' ' -f2)
cd lib/drink
if [ $version -ge 26 ]; then
    ln -sf edns-erlang-after-or-equal-26.ex-link-it edns.ex
    cd ../../test
    ln -sf edns-test-erlang-after-or-equal-26.ex-link-it drink_edns_test.exs
else
    ln -sf edns-erlang-before-26.ex-link-it edns.ex
    cd ../../test
    ln -sf edns-test-erlang-before-26.ex-link-it drink_edns_test.exs
fi
cd ..

